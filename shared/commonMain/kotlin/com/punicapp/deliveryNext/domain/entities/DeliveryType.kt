package com.punicapp.deliveryNext.domain.entities

import kotlinx.serialization.Serializable

@Serializable
enum class DeliveryType(val id: Int) {
    DELIVERY(1),
    PICKUP(2),
    BOTH(3)
}
package com.punicapp.deliveryNext.domain.entities

import kotlinx.serialization.Serializable

@Serializable
class Basket(val products: MutableList<BasketItem>) {
    val cartSum get() = products.filterIsInstance<BasketProduct>().sumByDouble { it.product.price * it.quantity }
    val orderSum get() = products.sumByDouble { it.fullPrice }
    val productCount get() = products.filterIsInstance<BasketProduct>().sumBy { it.quantity }

    fun findProductById(id: Long): BasketProduct? = products.filterIsInstance<BasketProduct>().firstOrNull { it.id == id }
    fun findProductByIdAndAddons(id: Long, selectedAddons: Map<Long?, MutableList<Addon?>>?): BasketProduct? = products.filterIsInstance<BasketProduct>().firstOrNull {
        it.id == id && it.selectedAddons?.equals(selectedAddons) == true
    }

}
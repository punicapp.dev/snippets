package com.punicapp.deliveryNext.domain.entities

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class OrderItem(
        @SerialName("id")
        val id: Long,
        @SerialName("quantity")
        val quantity: Int,
        @SerialName("name")
        val name: String,
        @SerialName("price")
        val price: Double,
        @SerialName("selectedaddons")
        val selectedaddons: List<Selectedaddon>? = null
)

@Serializable
data class OrderItemWithSelected(
        @SerialName("id")
        val id: Long,
        @SerialName("quantity")
        val quantity: Int,
        @SerialName("name")
        val name: String,
        @SerialName("price")
        val price: Double,
        @SerialName("selectedaddons")
        val selectedaddons: List<Selectedaddon>?
)

@Serializable
data class Selectedaddon(
        @SerialName("addon_id")
        val addonId: Long,
        @SerialName("addon_category_id")
        val addonCategoryId: Long
)

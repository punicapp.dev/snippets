package com.punicapp.deliveryNext.domain.entities

class AddonManager {
    private val selectedAddons = mutableMapOf<Long?, MutableList<Addon?>>()

    fun getAddons() = selectedAddons

    fun resetAddons() {
        selectedAddons.clear()
    }

    fun editAddon(addon: Addon, data: AddonCategory, selected: Boolean) {
        var addons = (selectedAddons[data.id] ?: mutableListOf())
        when (data.type) {
            AddonCategoryType.MULTI -> {
                if (selected) {
                    addons.add(addon)
                    selectedAddons[data.id] = addons
                } else {
                    addons.remove(addon)
                }
            }
            AddonCategoryType.SINGLE -> {
                if (selected) {
                    addons = mutableListOf(addon)
                    selectedAddons[data.id] = addons
                } else {
                    selectedAddons.remove(data.id)
                }
            }
        }
    }
}
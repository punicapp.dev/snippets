package com.punicapp.deliveryNext.domain.entities

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Restaurant(
        val id: Int,
        val name: String,
        val description: String,
        val slug: String,
        @SerialName("location_id") val locationId: String? = null,
        @SerialName("image") val imageUrl: String,
        @SerialName("placeholder_image") val placeholderImageUrl: String,
        val rating: String? = null,
        @SerialName("delivery_time") val deliveryTime: String,
        @SerialName("price_range") val priceRange: String,
        @SerialName("is_pureveg") val isPureVeg: Int,
        val latitude: String,
        val longitude: String,
        @SerialName("restaurant_charges") val restaurantCharges: String? = null,
        @SerialName("delivery_charges") val deliveryCharges: String? = null,
        @SerialName("commission_rate") val commissionRate: String,
        @SerialName("min_order_price") val minOrderPrice: String,
        val address: String,
        val sku: String,
        @SerialName("is_active") val isActive: Int,
        @SerialName("is_accepted") val isAccepted: Int,
        @SerialName("is_featured") val isFeatured: Int,
        @SerialName("delivery_type") val deliveryType: Int?,
        @SerialName("delivery_radius") val deliveryRadius: String,
        @SerialName("delivery_charge_type") val deliveryChargeType: String,
        @SerialName("base_delivery_charge") val baseDeliveryCharge: String? = null,
        @SerialName("base_delivery_distance") val baseDeliveryDistance: String? = null,
        @SerialName("extra_delivery_charge") val extraDeliveryCharge: String? = null,
        @SerialName("extra_delivery_distance") val extraDeliveryDistance: String? = null,
        val pincode: String? = null,
        val landmark: String? = null,
        val certificate: String? = null,
        @SerialName("pickup_addresses") val pickupAddresses: List<String>? = null
) {
    fun getDeliveryType(): DeliveryType {
        return when (deliveryType) {
            1 -> DeliveryType.DELIVERY
            2 -> DeliveryType.PICKUP
            3 -> DeliveryType.BOTH
            else -> DeliveryType.DELIVERY
        }
    }
}
/*
{
    "id": 1,
    "name": "Restik",
    "description": "Restik Restik Restik Restik",
    "location_id": null,
    "image": "/assets/img/restaurants/1588245684yY4fFS6uGj.png",
    "rating": "5",
    "delivery_time": "5",
    "price_range": "5",
    "is_pureveg": 1,
    "slug": "restik",
    "placeholder_image": "/assets/img/restaurants/small/1588245684yY4fFS6uGj-sm.png",
    "latitude": "54.98076625",
    "longitude": "83.01269514",
    "certificate": null,
    "restaurant_charges": null,
    "delivery_charges": "1.00",
    "address": "г Новосибирск, ул Энгельса, д 3, кв 96",
    "pincode": null,
    "landmark": null,
    "sku": "1588245684BzV0ELBLr9",
    "is_active": 0,
    "is_accepted": 1,
    "is_featured": 0,
    "commission_rate": "10.00",
    "delivery_type": 1,
    "delivery_radius": "10.00",
    "delivery_charge_type": "FIXED",
    "base_delivery_charge": null,
    "base_delivery_distance": null,
    "extra_delivery_charge": null,
    "extra_delivery_distance": null,
    "min_order_price": "10.00"
}
*/
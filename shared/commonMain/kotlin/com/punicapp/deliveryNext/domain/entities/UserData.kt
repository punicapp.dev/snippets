package com.punicapp.deliveryNext.domain.entities

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class UserData(
        @SerialName("house")
        val house: String?,
        @SerialName("address")
        val address: String?,
        @SerialName("latutude")
        val lat: Double? = null,
        @SerialName("longitude")
        val lng: Double? = null,
        @SerialName("name")
        val name: String,
        @SerialName("email")
        val email: String,
        @SerialName("phone")
        val phoneNumber: String
)

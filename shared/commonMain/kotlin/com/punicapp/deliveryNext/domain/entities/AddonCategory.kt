package com.punicapp.deliveryNext.domain.entities

import kotlinx.serialization.Serializable

@Serializable
data class AddonCategory(
        val id: Long? = null,
        val name: String? = null,
        val type: AddonCategoryType? = null,
        val addons: List<Addon>? = null
)



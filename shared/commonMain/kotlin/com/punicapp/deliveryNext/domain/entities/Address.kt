package com.punicapp.deliveryNext.domain.entities

import kotlinx.serialization.Serializable

@Serializable
class Address(
    val id: String,
    val address: String?,
    val flat: String?,
    val porch: String?,
    val floor: String?
)
package com.punicapp.deliveryNext.domain.entities

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class Confirmation(
        @SerialName("type")
        val type: String,
        @SerialName("confirmation_url")
        val confirmationUrl: String
)
package com.punicapp.deliveryNext.domain.entities

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class OrderData(
        val name: String?,
        val phone: String?,
        val email: String?,
        val address: Address?,
        val comment: String?,
        val payment: PaymentType,
        val delivery: DeliveryType,
        val latitude: Double? = null,
        val longitude: Double? = null,
        @SerialName("pickup_address") val pickupAddress: String? = null
)
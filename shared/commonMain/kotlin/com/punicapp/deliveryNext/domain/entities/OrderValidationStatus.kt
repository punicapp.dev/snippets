package com.punicapp.deliveryNext.domain.entities

enum class OrderValidationStatus {
    NameNotValid, PhoneNotValid, EmailNotValid, AddressNotValid
}
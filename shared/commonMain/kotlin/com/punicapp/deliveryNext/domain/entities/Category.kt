package com.punicapp.deliveryNext.domain.entities

data class ProductCategory(
    val id: String,
    val name: String
)
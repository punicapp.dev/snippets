package com.punicapp.deliveryNext.domain.entities

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class Payment(
        @SerialName("paid")
        val paid: Boolean?,
        @SerialName("id")
        val id: String?,
        @SerialName("status")
        val status: String?,
        @SerialName("confirmation")
        val confirmation: Confirmation? = null
) {
    val isCanceled get() = status == PaymentStatuses.CANCELED.lowCaseName
}
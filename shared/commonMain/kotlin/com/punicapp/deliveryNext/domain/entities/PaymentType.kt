package com.punicapp.deliveryNext.domain.entities

enum class PaymentType {
    COD,
    CARD
}
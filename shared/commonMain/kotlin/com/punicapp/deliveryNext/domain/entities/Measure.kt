package com.punicapp.deliveryNext.domain.entities

import kotlinx.serialization.Serializable
import kotlin.math.roundToInt

@Serializable
data class Measure(
    val amount: Double,
    val unit: String
)

fun Measure.toDisplayFormat(): String {
    return if (amount > 0)
        "${amount.roundToInt()} $unit"
    else ""
}
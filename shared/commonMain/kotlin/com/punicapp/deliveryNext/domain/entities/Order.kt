package com.punicapp.deliveryNext.domain.entities

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Order(
        @SerialName("unique_order_id")
        val orderId: String,
        @SerialName("id")
        val id: Long,
        @SerialName("user_id")
        val userId: Long,
        @SerialName("orderstatus_id")
        val orderStatusId: Int,
        @SerialName("location")
        val location: String? = null,
        @SerialName("address")
        val address: String? = null,
        @SerialName("restaurant_charge")
        val restaurantCharge: Double? = null,
        @SerialName("delivery_charge")
        val deliveryCharge: Double? = null,
        @SerialName("payable")
        val payable: Double? = null,
        @SerialName("total")
        val total: Double? = null,
        @SerialName("order_comment")
        val comment: String? = null,
        @SerialName("email")
        val email: String? = null,
        @SerialName("name")
        val name: String? = null,
        @SerialName("phone")
        val phone: String? = null,
        @SerialName("payment_mode")
        val paymentMode: String? = null,
        @SerialName("restaurant_id")
        val restaurantId: String? = null,
        @SerialName("delivery_type")
        val deliveryType: Int? = null,
        @SerialName("updated_at")
        val updatedAt: String? = null,
        @SerialName("created_at")
        val createdAt: String? = null,
        @SerialName("products")
        var products: List<BasketProduct>? = null,
        @SerialName("pickup_address")
        var pickupAddress: String? = null
) {
        fun getDeliveryType(): DeliveryType {
                return when (deliveryType) {
                        1 -> DeliveryType.DELIVERY
                        2 -> DeliveryType.PICKUP
                        3 -> DeliveryType.BOTH
                        else -> DeliveryType.DELIVERY
                }
        }
}
package com.punicapp.deliveryNext.domain.entities

import kotlinx.serialization.Serializable

@Serializable
enum class AddonCategoryType {
    MULTI,
    SINGLE
}
package com.punicapp.deliveryNext.domain.entities

import kotlinx.serialization.Serializable

@Serializable
data class AdditionalOption(
        val id: String,
        val name: String,
        val price: Double
)
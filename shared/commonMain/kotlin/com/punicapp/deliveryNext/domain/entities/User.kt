package com.punicapp.deliveryNext.domain.entities

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class User(
        @SerialName("data")
        val userData: UserData
)
package com.punicapp.deliveryNext.domain.entities

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class PaymentBody(
        @SerialName("order_id")
        val orderId: String,
        @SerialName("payment_token")
        val paymentToken: String
)
package com.punicapp.deliveryNext.domain.entities

enum class PaymentStatuses(val lowCaseName: String) {
    SUCCESS("succeeded"),
    CANCELED("canceled")
}
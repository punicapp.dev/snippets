package com.punicapp.deliveryNext.domain.entities

import kotlinx.serialization.Serializable

@Serializable
sealed class BasketItem {
    abstract val sortOrder: Int
    abstract val singlePrice: Double
    abstract val fullPrice: Double
}

@Serializable
data class DeliveryItem(override var singlePrice: Double) : BasketItem() {
    override val sortOrder: Int get() = 1
    override val fullPrice: Double get() = singlePrice
}

@Serializable
data class BasketProduct(
        var product: Product,
        var quantity: Int = 1,
        val selectedAddons: Map<Long?, MutableList<Addon?>>? = null
) : BasketItem() {
    override val sortOrder: Int get() = 0
    override val singlePrice: Double get() = product.price
    override val fullPrice: Double get() {
        var addonsPrice: Double = 0.0
        selectedAddons?.values?.flatten()?.let {
            for (addon in it) {
                addonsPrice += (addon?.price ?: 0.0) * quantity
            }
        }

        return product.price * quantity + addonsPrice
    }

    val id: Long get() = product.id

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is BasketProduct) return false

        if (id != other.id) return false
        if (selectedAddons?.equals(other.selectedAddons) != true) return false
        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}

package com.punicapp.deliveryNext.domain.entities

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/*
                  {
                                "id": 1,
                                "name": "тестовые доплнения",
                                "price": 129,
                                "addon_category_id": 1,
                                "user_id": 4,
                                "created_at": "2020-05-25 16:17:21",
                                "updated_at": "2020-05-26 09:40:32"
                            }
 */
@Serializable
data class Addon(
        val id: Long? = null,
        val name: String? = null,
        val price: Double? = null,
        @SerialName("addon_category_id")
        val addoncategoryid: Long? = null
)
package com.punicapp.deliveryNext.domain.entities

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/*
                "id": 1,
                "restaurant_id": 1,
                "item_category_id": 1,
                "name": "Маргарита",
                "price": "199.00",
                "old_price": "0.00",
                "image": "/assets/img/items/1588247086810xK6nPiI.jpg",
                "is_recommended": 0,
                "is_popular": 0,
                "is_new": 1,
                "desc": "<p><span style=\"font-size: 14.4px;\">Маргарита</span><br></p>",
                "placeholder_image": "/assets/img/items/small/1588247086810xK6nPiI-sm.jpg",
                "is_active": 1,
                "is_veg": 0,
                "category_name": "Пицца",
                "addon_categories": []
 */
@Serializable
data class Product(
        val id: Long,
        @SerialName("item_category_id")
        val categoryId: String,
        val name: String,
        val image: String,
        val price: Double,
        @SerialName("desc")
        val description: String? = null,
        @SerialName("category_name")
        val categoryName: String? = null,
        @SerialName("old_price")
        val oldPrice: Double? = null,
        @SerialName("restaurant_id")
        val restaurantId: Int? = null,
        @SerialName("placeholder_image")
        val placeholderImage: String? = null,
        @SerialName("dimension")
        val measure: Measure,
        @SerialName("addon_categories")
        val addonCategories: List<AddonCategory>? = null
) {
    val uniqueId: String get() = "${categoryId}_${id}"
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Product) return false

        if (id != other.id) return false
        if (categoryId != other.categoryId) return false
        if (uniqueId != other.uniqueId) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + categoryId.hashCode()
        result = 31 * result + uniqueId.hashCode()
        return result
    }
}

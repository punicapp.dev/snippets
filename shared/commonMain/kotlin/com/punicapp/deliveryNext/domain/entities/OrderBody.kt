package com.punicapp.deliveryNext.domain.entities

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class OrderBody(
        @SerialName("user")
        val user: User,
        @SerialName("restaurant_id")
        val restaurantId: Int,
        @SerialName("order")
        val order: List<OrderItem>,
        @SerialName("method")
        val method: String,
        @SerialName("order_comment")
        val comment: String? = null,
        @SerialName("delivery_type")
        val deliveryType: Int,
        @SerialName("sdk")
        val sdk: Boolean = true,
        @SerialName("pickup_address") val pickupAddress: String? = null
)
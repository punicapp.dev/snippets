package com.punicapp.deliveryNext.domain.remote

import kotlinx.serialization.*
//import kotlinx.serialization.internal.SerialClassDescImpl
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.JsonInput
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.content

@Serializable
data class ErrorItemParameter(val fieldName: String? = null)

@Serializable
data class ErrorItem(
        val message: String? = null,
        val parameters: ErrorItemParameter? = null)

@Serializable
data class ApiError(val code: Int, val description: String, val items: List<ErrorItem>? = null)

@Serializable
data class ApiResult(val status: Boolean, val error: ApiError? = null)
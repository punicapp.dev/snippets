package com.punicapp.deliveryNext.domain.remote.response

import com.punicapp.deliveryNext.domain.entities.Payment
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class PaymentResponse(
        @SerialName("success")
        val result: Boolean = false,
        @SerialName("data")
        val data: Payment? = null,
        @SerialName("errors")
        val errors: List<String?> = mutableListOf()
)
package com.punicapp.deliveryNext.domain.remote.response

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class TestResponse(val lat: Double? = null,
                   val lon: Double? = null,
                   val current: Weather? = null,
                   val hourly: List<Weather>? = null)


@Serializable
data class Weather(val sunrise: Int? = null,
               val sunset: Int? = null,
               val temp: Double? = null,
               @SerialName("feels_like") val feelsLike: Double? = null)


/*
{
    "lat": 60.99,
    "lon": 30.9,
    "timezone": "Europe/Moscow",
    "current": {
    "dt": 1588152487,
    "sunrise": 1588125216,
    "sunset": 1588182815,
    "temp": 273.71,
    "feels_like": 265.26,
    "pressure": 998,
    "humidity": 85,
    "dew_point": 271.68,
    "uvi": 3.2,
    "clouds": 100,
    "wind_speed": 8.9,
    "wind_deg": 35,
    "weather": [
    {
        "id": 804,
        "main": "Clouds",
        "description": "overcast clouds",
        "icon": "04d"
    }
    ]
},
    "hourly": [
    {
        "dt": 1588150800,
        "temp": 273.71,
        "feels_like": 265.26,
        "pressure": 998,
        "humidity": 85,
        "dew_point": 271.68,
        "clouds": 100,
        "wind_speed": 8.9,
        "wind_deg": 35,
        "weather": [
        {
            "id": 804,
            "main": "Clouds",
            "description": "overcast clouds",
            "icon": "04d"
        }
        ]
    },

        */
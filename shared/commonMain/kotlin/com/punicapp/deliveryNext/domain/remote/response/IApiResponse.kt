package com.punicapp.deliveryNext.domain.remote.response

import com.punicapp.deliveryNext.domain.remote.ApiResult
import com.punicapp.deliveryNext.domain.remote.ErrorItem

interface IApiResponse {
    val result: ApiResult?

    fun getError(): String? = result?.error?.description

    fun getErrorItems(): List<ErrorItem>? = result?.error?.items
}
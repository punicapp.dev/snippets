package com.punicapp.deliveryNext.domain.remote

import com.punicapp.deliveryNext.sharedUtils.ApplicationDispatcher
import com.punicapp.deliveryNext.sharedUtils.callCb
import com.punicapp.deliveryNext.sharedUtils.wrapInResult
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

sealed class AppResult<T> {
    data class Ok<T>(val data: T) : AppResult<T>()
    data class Error<T>(val error: Throwable) : AppResult<T>()

    fun <U> map(mapper: (T) -> U): AppResult<U> = when (this) {
        is Ok -> Ok(mapper(data))
        is Error -> Error(error)
    }
}

class AsyncAppResult<T>
private constructor(private val funcWithCb: ((AppResult<T>) -> Unit) -> Unit) {
    companion object {
        internal fun <T> fromSuspend(targetFunc: suspend () -> T): AsyncAppResult<T> =
            AsyncAppResult { callback ->
                GlobalScope
                    .launch(ApplicationDispatcher) {
                        val result: AppResult<T> = wrapInResult { targetFunc() }
                        callCb(callback, result)
                    }
            }

        // TODO: unused?
        fun <T> create(targetFunc: ((T) -> Unit) -> Unit): AsyncAppResult<T> =
            AsyncAppResult { emitter ->
                try {
                    targetFunc { t -> emitter(AppResult.Ok(t)) }
                } catch (e: Throwable) {
                    emitter(AppResult.Error(e))
                }
            }
    }

    fun subscribe(callback: (AppResult<T>) -> Unit) {
        funcWithCb(callback)
    }
}
package com.punicapp.deliveryNext.domain.remote.response

import com.punicapp.deliveryNext.domain.entities.Product
import kotlinx.serialization.Serializable

@Serializable
data class CatalogResponse(val recommended: List<Recomend>? = null,
                           val items: Map<String, List<Product>>)

@Serializable
data class Recomend(val id: Int? = null)
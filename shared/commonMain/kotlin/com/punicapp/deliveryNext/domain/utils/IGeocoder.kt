package com.punicapp.deliveryNext.domain.utils

import com.punicapp.deliveryNext.domain.entities.GeocoderAddress

interface IGeocoder {
    fun geocode(lat: Double, lng: Double, cb: (GeocoderAddress?) -> Unit)
}
package com.punicapp.deliveryNext.domain.utils

import com.punicapp.deliveryNext.data.storage.IKeyValueRepository
import kotlinx.serialization.ImplicitReflectionSerializer
import kotlinx.serialization.KSerializer
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import kotlinx.serialization.parse
import kotlinx.serialization.parseList

val localSerializationJson: Json by lazy { Json(JsonConfiguration(ignoreUnknownKeys = true, encodeDefaults = false)) }

@UseExperimental(ImplicitReflectionSerializer::class)
inline fun <reified T : Any> IKeyValueRepository.getSerializedObject(key: String): T? =
        getString(key)?.let { localSerializationJson.parse(it) }

@UseExperimental(ImplicitReflectionSerializer::class)
inline fun <reified T : Any> IKeyValueRepository.getSerializedList(key: String): List<T>? =
        getString(key)?.let { localSerializationJson.parseList(it) }

@UseExperimental(ImplicitReflectionSerializer::class)
inline fun <reified T : Any> IKeyValueRepository.putSerializedObject(key: String, value: T, serializer: KSerializer<T>) =
        putString(key, localSerializationJson.stringify(serializer, value))
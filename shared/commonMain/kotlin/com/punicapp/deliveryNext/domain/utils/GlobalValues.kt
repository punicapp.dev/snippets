package com.punicapp.deliveryNext.domain.utils

const val CATALOG_PAGE_SIZE = 8
const val SEARCH_PAGE_SIZE = 8
const val DEFAULT_COUNTRY = "Россия"
const val DEFAULT_CITY = "Москва"
const val MAIN_PAGE_PRODUCT_LIMIT = 30
const val MAIN_PAGE_BANNER_LIMIT = 4
const val AR_PRODUCT_LIST = "AR_PRODUCT_LIST"
const val AR_COSMETICS = "AR_COSMETICS"
val RESTRICTED_CATEGORIES = listOf("99", "97")

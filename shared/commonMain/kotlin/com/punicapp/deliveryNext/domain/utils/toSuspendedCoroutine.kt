package com.punicapp.deliveryNext.domain.utils

import com.punicapp.deliveryNext.domain.remote.AppResult
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

suspend fun <T> (((AppResult<T>) -> Unit) -> Unit).toSuspendedCoroutine() = suspendCoroutine<T> { cont ->
    this { res ->
        when (res) {
            is AppResult.Ok -> cont.resume(res.data)
            is AppResult.Error -> cont.resumeWithException(res.error)
        }
    }
}
package com.punicapp.deliveryNext.domain.utils

import com.punicapp.deliveryNext.domain.entities.DeliveryType
import com.punicapp.deliveryNext.domain.entities.OrderData
import com.punicapp.deliveryNext.domain.entities.OrderValidationStatus

enum class OrderValidator {
    Default {
        override fun validateOrder(data: OrderData): List<OrderValidationStatus> = listOf(
                data.name.isNullOrBlank() to OrderValidationStatus.NameNotValid,
                (data.phone?.let { it.length != PHONE_LENGTH } != false) to OrderValidationStatus.PhoneNotValid,
                (data.email?.let { !EMAIL_PATTERN.matches(it) } != false) to OrderValidationStatus.EmailNotValid,
                (data.delivery == DeliveryType.DELIVERY && data.address?.address.isNullOrBlank()) to OrderValidationStatus.AddressNotValid,
                (data.delivery != DeliveryType.DELIVERY && data.pickupAddress.isNullOrBlank()) to OrderValidationStatus.AddressNotValid
        ).mapNotNull { if (it.first) it.second else null }
    };

    companion object {
        const val PHONE_LENGTH = 18
        val EMAIL_PATTERN = Regex("[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                "\\@" +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                "(" +
                "\\." +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                ")+")
    }

    abstract fun validateOrder(data: OrderData): List<OrderValidationStatus>
}
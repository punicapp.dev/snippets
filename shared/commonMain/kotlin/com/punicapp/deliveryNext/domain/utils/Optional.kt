package com.punicapp.deliveryNext.domain.utils

sealed class Optional<T: Any> {
    companion object {
        fun <T: Any> fromNullable(t: T?) = if (t != null) Some(t) else None<T>()
    }

    abstract fun or(value: T): T
    abstract fun or(producer: () -> T): T
    abstract fun orNull(): T?
    abstract fun get(): T
    abstract fun isPresent(): Boolean

    data class Some<T: Any>(private val thisValue: T) : Optional<T>() {
        override fun or(value: T) = thisValue
        override fun or(producer: () -> T): T = thisValue
        override fun orNull(): T? = thisValue
        override fun get(): T = thisValue
        override fun isPresent(): Boolean = true
    }

    class None<T: Any> : Optional<T>() {
        override fun or(value: T): T = value
        override fun or(producer: () -> T): T = producer()
        override fun orNull(): T? = null
        override fun get(): T = throw RuntimeException("Call get() on Optional.None")
        override fun isPresent(): Boolean = false
    }
}
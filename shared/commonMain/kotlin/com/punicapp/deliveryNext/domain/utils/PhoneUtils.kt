package com.punicapp.deliveryNext.domain.utils

object PhoneUtils {
    private const val SEVEN = "7"
    private const val PHONE_LENGTH = 11

    fun isPhoneValid(s: String?): Boolean {
        if (s.isNullOrBlank())
            return false

        val value = s.replaceFirst("\\+7".toRegex(), SEVEN)
        return value.matches("[78] ?\\(?[0-9]*\\)? ?[0-9]*-?[0-9]*-?[0-9]*".toRegex()) &&
                value.replace("[\\D]".toRegex(), "").length == PHONE_LENGTH
    }
}
package com.punicapp.deliveryNext.domain.usecases

import com.punicapp.deliveryNext.domain.entities.*
import com.punicapp.deliveryNext.domain.remote.AsyncAppResult

interface IBasketUseCase {
    suspend fun getBasketSync(): Basket
    fun getRestaurant(): Restaurant?
    fun getBasket(): AsyncAppResult<Basket>
    fun editBasket(product: Product, replace: Boolean = true): AsyncAppResult<Basket>
    fun deleteProduct(product: Product, addons: Map<Long?, MutableList<Addon?>>?): AsyncAppResult<Basket>

    fun clearBasket(): AsyncAppResult<Basket>
    fun getDeliveryTime(): AsyncAppResult<String>

    fun increaseCounter(): Int
    fun decreaseCounter(): Int
    fun resetCounter(): Int

    fun increaseCount(basketProduct: BasketProduct): AsyncAppResult<Basket>
    fun decreaseCount(basketProduct: BasketProduct): AsyncAppResult<Basket>

    fun editAddon(addon: Addon, data: AddonCategory, selected: Boolean)
    fun resetAddons()
}
package com.punicapp.deliveryNext.domain.usecases.impl

import com.punicapp.deliveryNext.data.api.IApiService
import com.punicapp.deliveryNext.domain.usecases.IGeneralUseCase

class GeneralUseCaseImpl(private val api: IApiService) : IGeneralUseCase {
    override fun provideContactsLink(): String {
        return api.getContactsLink()
    }

    override fun provideDeliveryTermsLink(): String {
        return api.getDeliveryTermsLink()
    }
}
package com.punicapp.deliveryNext.domain.usecases.impl

import com.punicapp.deliveryNext.data.api.ApiConfig
import com.punicapp.deliveryNext.data.api.DeliveryApi
import com.punicapp.deliveryNext.data.api.IApiService
import com.punicapp.deliveryNext.data.storage.IRepository
import com.punicapp.deliveryNext.domain.entities.ProductCategory
import com.punicapp.deliveryNext.domain.entities.Product
import com.punicapp.deliveryNext.domain.entities.Restaurant
import com.punicapp.deliveryNext.domain.remote.AsyncAppResult
import com.punicapp.deliveryNext.domain.remote.response.CatalogResponse
import com.punicapp.deliveryNext.domain.usecases.ISyncUseCase
import com.punicapp.deliveryNext.sharedUtils.ApplicationDispatcher
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class SyncUseCaseImpl(
    private val repoProducts: IRepository<Product>,
    private val repoCategories: IRepository<ProductCategory>,
    private val repoRestaurant: IRepository<Restaurant>,
    private val api: IApiService
) : ISyncUseCase {
    override fun sync(): AsyncAppResult<Unit> =
        AsyncAppResult.fromSuspend {
            val catalogModel = api.getCatalog()
            processCatalog(catalogModel)

            val restaurant = api.getRestaurantInfo()
            processRestaurant(restaurant)
        }

    private fun processCatalog(catalog: CatalogResponse) {
        println("processCatalogStructure")
        // save categories
        val categories: MutableList<ProductCategory> = mutableListOf()
        val iterator = catalog.items.keys.iterator()
        iterator.forEach {
            categories += ProductCategory(it, it)
        }
        repoCategories.save(categories)
        println("categories $categories")


        var products: List<Product> = mutableListOf()
        catalog.items.values.iterator().forEach {
            it.iterator().forEach {
                products += it
            }
        }

        println("products $products")
        repoProducts.save(products)


        // checking values

        println("repoProducts getList ${repoProducts.getList()}")
        println("repoCategories getList ${repoCategories.getList()}")


    }

    private fun processRestaurant(restaurant: Restaurant) {
        println("restaurant = ${restaurant}")

        repoRestaurant.save(listOf(restaurant))

        println("repoRestaurant getList ${repoRestaurant.getList()}")

    }


}
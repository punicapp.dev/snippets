package com.punicapp.deliveryNext.domain.usecases

import com.punicapp.deliveryNext.domain.remote.AsyncAppResult

interface  ISyncUseCase {
    fun sync(): AsyncAppResult<Unit>
}
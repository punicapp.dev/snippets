package com.punicapp.deliveryNext.domain.usecases.impl

import com.punicapp.deliveryNext.data.storage.IKeyValueRepository
import com.punicapp.deliveryNext.data.storage.IRepository
import com.punicapp.deliveryNext.domain.entities.*
import com.punicapp.deliveryNext.domain.remote.AsyncAppResult
import com.punicapp.deliveryNext.domain.usecases.IBasketUseCase
import com.punicapp.deliveryNext.domain.utils.Optional
import com.punicapp.deliveryNext.domain.utils.getSerializedObject
import com.punicapp.deliveryNext.domain.utils.putSerializedObject

class BasketUseCaseImpl(
        private val basketRepo: IKeyValueRepository,
        private val restaurantRepo: IRepository<Restaurant>
) : IBasketUseCase {
    companion object {
        const val BASKET = "basket"

        private const val MAX_LIMIT = 99
        private const val MIN_LIMIT = 1
    }

    private var count: Int = MIN_LIMIT
    private val addonManager = AddonManager()

    override fun getRestaurant(): Restaurant? =
            restaurantRepo.getFirst { true }

    override suspend fun getBasketSync(): Basket =
            basketRepo.getSerializedObject(BASKET) ?: Basket(mutableListOf())

    override fun getBasket(): AsyncAppResult<Basket> = AsyncAppResult.fromSuspend { getBasketSync() }

    private fun editBasketSync(product: Product, selectedAddons: Map<Long?, MutableList<Addon?>>?, quantityModifier: (Int) -> Int): AsyncAppResult<Basket> =
            AsyncAppResult.fromSuspend {
                val basket = getBasketSync()
                val bProduct = BasketProduct(product, quantityModifier(0), selectedAddons)
                val newProduct = basket.findProductByIdAndAddons(bProduct.id, selectedAddons)?.let {
                    it.quantity = quantityModifier(it.quantity)
                }
                when {
                    newProduct == null && bProduct.quantity > 0 -> basket.products.add(bProduct)
                    bProduct.quantity == 0 -> {
                        basket.products.remove(bProduct)
                        if (basket.products.filterIsInstance<BasketProduct>().isEmpty())
                            basket.products.clear()
                    }
                }
                val deliveryPrice = getDeliveryPriceValue()
                var basketProduct: DeliveryItem? = basket.products.filterIsInstance<DeliveryItem>().firstOrNull()
                if (basketProduct == null && basket.products.isNotEmpty()) {
                    basketProduct = DeliveryItem(deliveryPrice)
                    basket.products.add(basketProduct)
                }

                basketProduct?.singlePrice = deliveryPrice

                basket.products.sortBy { it.sortOrder }
                basketRepo.putSerializedObject(BASKET, basket, Basket.serializer())
                basket
            }

    override fun editBasket(product: Product, replace: Boolean): AsyncAppResult<Basket> = editBasketSync(product, addonManager.getAddons()) {
        if (replace) {
            count
        } else {
            it + count
        }
    }

    override fun deleteProduct(product: Product, addons: Map<Long?, MutableList<Addon?>>?): AsyncAppResult<Basket> = editBasketSync(product, addons) {
        0
    }

    override fun clearBasket(): AsyncAppResult<Basket> = AsyncAppResult.fromSuspend {
        basketRepo.remove(BASKET)
        Basket(mutableListOf())
    }

    override fun getDeliveryTime(): AsyncAppResult<String> = AsyncAppResult.fromSuspend {
        "${Optional.fromNullable(restaurantRepo.getFirst { true }).get().deliveryTime} мин"
    }

    private fun getDeliveryPriceValue() = Optional.fromNullable(restaurantRepo.getFirst { true }).get().deliveryCharges?.toDouble()
            ?: 0.0

    override fun increaseCounter(): Int {
        if (count < MAX_LIMIT) {
            count++
        }

        return count
    }

    override fun decreaseCounter(): Int {
        if (count > MIN_LIMIT) {
            count--
        }

        return count
    }

    override fun resetCounter(): Int {
        count = MIN_LIMIT
        return count
    }

    override fun editAddon(addon: Addon, data: AddonCategory, selected: Boolean) {
        addonManager.editAddon(addon, data, selected)
    }

    override fun resetAddons() {
        addonManager.resetAddons()
    }
    
    override fun increaseCount(basketProduct: BasketProduct): AsyncAppResult<Basket> {
        return updateProductCount(basketProduct, ChangeCountType.INCREASE)
    }

    override fun decreaseCount(basketProduct: BasketProduct): AsyncAppResult<Basket> {
        return updateProductCount(basketProduct, ChangeCountType.DECREASE)
    }

    private enum class ChangeCountType {
        INCREASE,
        DECREASE,
    }

    private fun updateProductCount(basketProduct: BasketProduct, changeCountType: ChangeCountType): AsyncAppResult<Basket> {
        return AsyncAppResult.fromSuspend {
            val basket = basketRepo.getSerializedObject(BASKET) ?: Basket(mutableListOf())

            val product = if (basketProduct.selectedAddons != null) {
                basket.findProductByIdAndAddons(basketProduct.id, basketProduct.selectedAddons)
            } else {
                basket.findProductById(basketProduct.id)
            }
            if (product != null) {
                when (changeCountType) {
                    ChangeCountType.INCREASE -> {
                        if (product.quantity < MAX_LIMIT) {
                            product.quantity++
                        }
                    }
                    ChangeCountType.DECREASE -> {
                        if (product.quantity > MIN_LIMIT) {
                            product.quantity--
                        }
                    }

                }
            }
            basket.products.sortBy { it.sortOrder }
            basketRepo.putSerializedObject(BASKET, basket, Basket.serializer())
            basket
        }
    }
}
package com.punicapp.deliveryNext.domain.usecases

interface IGeneralUseCase {
    fun provideContactsLink(): String
    fun provideDeliveryTermsLink(): String
}
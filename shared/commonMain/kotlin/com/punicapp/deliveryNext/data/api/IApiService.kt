package com.punicapp.deliveryNext.data.api

import com.punicapp.deliveryNext.domain.entities.*
import com.punicapp.deliveryNext.domain.remote.response.CatalogResponse

interface IApiService {
    suspend fun getCategories(): List<ProductCategory>
    suspend fun getProducts(categoryId: String): List<Product>
    suspend fun getCatalog(): CatalogResponse
    suspend fun getRestaurantInfo(): Restaurant
    suspend fun doOrder(orderData: OrderBody): Order
    suspend fun createPayment(paymentData: PaymentBody): Payment?
    fun getContactsLink(): String
    fun getDeliveryTermsLink(): String
}
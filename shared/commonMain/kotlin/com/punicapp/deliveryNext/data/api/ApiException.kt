package com.punicapp.deliveryNext.data.api

import com.punicapp.deliveryNext.domain.remote.ErrorItem

class ApiException(message: String, val code: Int, val errorItems: List<ErrorItem>? = null) : RuntimeException(message)
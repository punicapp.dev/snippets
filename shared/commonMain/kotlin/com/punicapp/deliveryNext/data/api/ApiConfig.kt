package com.punicapp.deliveryNext.data.api

class ApiConfig(
        val baseUrl: String,
        val slug: String,
        val headers: List<Pair<String, String>> = emptyList(),
        val isLogEnabled: Boolean = false
)
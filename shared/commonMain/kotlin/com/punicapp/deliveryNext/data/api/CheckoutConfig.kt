package com.punicapp.deliveryNext.data.api

class CheckoutConfig(
        val applicationId: String,
        val shopId: String
)
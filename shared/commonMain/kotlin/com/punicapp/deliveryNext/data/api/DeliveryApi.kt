package com.punicapp.deliveryNext.data.api

import com.punicapp.deliveryNext.data.api.mock.MockApiService
import com.punicapp.deliveryNext.domain.entities.*
import com.punicapp.deliveryNext.domain.remote.response.*
import io.ktor.client.HttpClient
import io.ktor.client.features.logging.DEFAULT
import io.ktor.client.features.logging.LogLevel
import io.ktor.client.features.logging.Logger
import io.ktor.client.features.logging.Logging
import io.ktor.client.request.*
import io.ktor.client.statement.HttpResponse
import io.ktor.client.statement.readText
import io.ktor.http.ContentType
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.http.content.TextContent
import kotlinx.serialization.KSerializer
import kotlinx.serialization.UnstableDefault
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration

class LoggerDelegate(val delegate: (String) -> Unit) : Logger {
    override fun log(message: String) {
        delegate(message)
    }
}

class DeliveryApi(
        private val config: ApiConfig,
        logFunction: ((String) -> Unit)? = null
) : IApiService by MockApiService() {
    @UnstableDefault
    private val noDefaultsJson by lazy {
        Json(
                JsonConfiguration(
                        encodeDefaults = false,
                        ignoreUnknownKeys = true,
                        isLenient = true
                )
        )
    }

    private fun HttpRequestBuilder.urlWithBase(endpoint: String) {
        url("${config.baseUrl}public/api/$endpoint")
    }

    private val client = HttpClient {
        if (config.isLogEnabled) install(Logging) {
            logger = logFunction?.let(::LoggerDelegate) ?: Logger.DEFAULT
            level = LogLevel.ALL
        }

        engine {

        }
    }.apply {
        requestPipeline.intercept(HttpRequestPipeline.Before) {
            config.headers.forEach {
                if (!context.headers.names().contains(it.first))
                    context.header(it.first, it.second)
            }
        }
    }

    private fun <T : IApiResponse> T.processStatus(statusCode: HttpStatusCode): T = apply {
        if (statusCode != HttpStatusCode.OK || result?.status != true) {
            throw ApiException(getError() ?: "Api error", statusCode.value, getErrorItems())
        }
    }

    private suspend fun HttpResponse.asString() = readText()

    private suspend inline fun request(
            method_: HttpMethod,
            url_: String,
            requestBuilder: (HttpRequestBuilder.() -> Unit) = {}
    ) =
            client.request<HttpResponse> {
                this.method = method_
                urlWithBase(url_)
                requestBuilder()
            }

    private suspend inline fun <T : IApiResponse> simpleRequest(
            method: HttpMethod, url: String, serializer: KSerializer<T>,
            requestBuilder: (HttpRequestBuilder.() -> Unit) = {}
    ): T {
        var httpResponse = request(method, url, requestBuilder)
        if (httpResponse.status == HttpStatusCode.Unauthorized) {
            httpResponse = request(method, url, requestBuilder)
        }
        val string = httpResponse.asString()
        return noDefaultsJson.parse(serializer, string).processStatus(httpResponse.status)
    }

    private suspend inline fun <T : TestResponse> simpleTestRequest(
            method: HttpMethod, url: String, serializer: KSerializer<T>,
            requestBuilder: (HttpRequestBuilder.() -> Unit) = {}
    ): T {
        println("simpleTestRequest")
        var httpResponse = request(method, url, requestBuilder)
        if (httpResponse.status == HttpStatusCode.Unauthorized) {
            httpResponse = request(method, url, requestBuilder)
        }
        val string = httpResponse.asString()
        return noDefaultsJson.parse(serializer, string)
    }

    override suspend fun getCatalog(): CatalogResponse {
        val data = client.post<String> {
            urlWithBase("get-restaurant-items/${config.slug}")
        }

        return noDefaultsJson.parse(CatalogResponse.serializer(), data)
    }

    override suspend fun getRestaurantInfo(): Restaurant {
        val data = client.post<String> {
            urlWithBase("get-restaurant-info/${config.slug}")
        }
        return noDefaultsJson.parse(Restaurant.serializer(), data)
    }

    override suspend fun doOrder(orderData: OrderBody): Order {
        val data = client.post<String> {
            urlWithBase("place-order")
            body = TextContent(noDefaultsJson.stringify(OrderBody.serializer(), orderData), ContentType.Application.Json)
        }

        return noDefaultsJson.parse(OrderResponse.serializer(), data).data
    }

    override suspend fun createPayment(paymentData: PaymentBody): Payment? {

        val data = client.post<String> {
            urlWithBase("payment/create-payment")
            body = TextContent(noDefaultsJson.stringify(PaymentBody.serializer(), paymentData), ContentType.Application.Json)
        }
        return noDefaultsJson.parse(PaymentResponse.serializer(), data).data
    }

    override fun getContactsLink(): String {
        return "http://${config.slug}.punictech.ru/restaurant-info/contact"
    }

    override fun getDeliveryTermsLink(): String {
        return "http://${config.slug}.punictech.ru/restaurant-info/delivery"
    }
}

package com.punicapp.deliveryNext.data.storage.impl

import com.punicapp.deliveryNext.data.storage.IRepository

class InMemoryRepository <T>: IRepository <T> {
    override fun getFirst(f: (T) -> Boolean): T? = getList().firstOrNull(f)

    override fun getFiltered(f: (T) -> Boolean): List<T>  = getList().filter(f)

    var objects: List<T> = mutableListOf()

    override fun save(objects: List<T>) {
        this.objects = objects
    }

    override fun getList() : List<T> {
        return objects
    }


}
package com.punicapp.deliveryNext.data.storage

interface IRepository <T> {
    fun save(objects: List<T>)
    fun getList() : List<T>
    fun getFiltered(f: (T)->Boolean) : List<T>
    fun getFirst(f: (T)->Boolean) : T?
}

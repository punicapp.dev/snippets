package com.punicapp.deliveryNext.sharedUtils


import com.punicapp.deliveryNext.data.api.ApiException
import com.punicapp.deliveryNext.domain.remote.AppResult
import kotlinx.coroutines.*

internal expect suspend fun <T> callCb(cb: (T) -> Unit, result: T)

inline fun <T> wrapInResult(f: () -> T) = try {
    AppResult.Ok(f())
} catch (t: Throwable) {
    AppResult.Error<T>(t)
}

internal fun <T> requestProcess(cb: (AppResult<T>) -> Unit, generate: suspend () -> T): Job {
    return GlobalScope.launch(ApplicationDispatcher) {
        val result: AppResult<T> = wrapInResult { generate() }
        callCb(cb, result)
    }
}
/*
internal fun <T> ISessionUseCase.authRequestProcess(cb: (AppResult<T>) -> Unit, generate: suspend (Boolean) -> T) {
    GlobalScope.launch(ApplicationDispatcher) {
        val authorized = isAuthorized()
        val result: AppResult<T> = try {
            val t = generate(authorized)
            AppResult.Ok(t)
        } catch (e: Throwable) {
            if (authorized && e.isUnauthError()) {
                try {
                    AppResult.Ok(generate(false))
                } catch (e: Throwable) {
                    AppResult.Error<T>(e)
                }
            } else AppResult.Error(e)
        }
        callCb(cb, result)
    }
}
*/
fun Throwable.isUnauthError(): Boolean = (this@isUnauthError as? ApiException)
        .let { it?.code == 500 || it?.code == 401 }

fun Throwable.isRefreshError(): Boolean = (this@isRefreshError as? ApiException)
        .let { it?.code == 500 || it?.code == 401 || it?.code == 400}

fun Throwable.isError(): Boolean = (this@isError as? ApiException)
        .let { it?.code == 500 || it?.code == 400}

internal suspend fun <T1, T2> collectData(request1: Deferred<T1?>, request2: Deferred<T2?>): Pair<AppResult<T1?>, AppResult<T2?>> {
    return appResult<T1>(request1) to appResult<T2>(request2)
}

private suspend fun <T> appResult(request: Deferred<T?>): AppResult<T?> {
    return wrapInResult { request.await() }
}

internal fun <T> runAsync(f: suspend CoroutineScope.() -> T) = GlobalScope.async(ApplicationDispatcher, block = f)

package com.punicapp.deliveryNext.sharedUtils

import kotlinx.coroutines.CoroutineDispatcher


internal expect val ApplicationDispatcher: CoroutineDispatcher

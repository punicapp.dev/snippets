package com.punicapp.deliveryNext.sharedUtils

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

internal actual suspend fun <T> callCb(cb: (T) -> Unit, result: T) {
    withContext(Dispatchers.Main) {
        cb(result)
    }
}

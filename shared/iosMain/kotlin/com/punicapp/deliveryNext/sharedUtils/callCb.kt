package com.punicapp.deliveryNext.sharedUtils

internal actual suspend fun <T> callCb(cb: (T) -> Unit, result: T) {
    cb(result)
}

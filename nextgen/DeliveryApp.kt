package com.punicapp.delivery.nextgen

import android.app.Application
import android.content.Context
import com.facebook.drawee.backends.pipeline.Fresco
import com.punicapp.delivery.nextgen.data.api.createApiModule
import com.punicapp.delivery.nextgen.data.api.createCheckoutModule
import com.punicapp.delivery.nextgen.data.repo.repositoryModule
import com.punicapp.delivery.nextgen.domain.useCasesModule
import com.punicapp.delivery.nextgen.presentation.about.aboutAppModule
import com.punicapp.delivery.nextgen.presentation.address.addressMapModule
import com.punicapp.delivery.nextgen.presentation.base.activityModule
import com.punicapp.delivery.nextgen.presentation.cart.cartModule
import com.punicapp.delivery.nextgen.presentation.cicerone.ciceroneModule
import com.punicapp.delivery.nextgen.presentation.menu.menuModule
import com.punicapp.delivery.nextgen.presentation.order.orderModule
import com.punicapp.delivery.nextgen.presentation.order_details.orderDetailsModule
import com.punicapp.delivery.nextgen.presentation.orders.ordersModule
import com.punicapp.delivery.nextgen.presentation.splash.splashModule
import com.punicapp.deliveryNext.data.api.ApiConfig
import com.punicapp.deliveryNext.data.api.CheckoutConfig
import com.punicapp.deliveryNext.domain.utils.OrderValidator
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

abstract class DeliveryApp : Application() {
    companion object {
        lateinit var context: Context
    }

    override fun onCreate() {
        super.onCreate()
        context = this@DeliveryApp
        initKoin()
        Fresco.initialize(applicationContext)
    }

    abstract fun provideApiConfig(): ApiConfig
    abstract fun provideCheckoutConfig(): CheckoutConfig
    open fun provideOnlyCash() = false
    open fun provideOrderValidator() = OrderValidator.Default
    open var provideVersion: String = ""


    private fun initKoin() {
        startKoin {
            androidContext(context)
            modules(
                    listOf(
                            activityModule,
                            ciceroneModule,
                            splashModule,
                            menuModule,
                            cartModule,
                            repositoryModule,
                            useCasesModule(provideOrderValidator()),
                            orderModule(provideOnlyCash()),
                            addressMapModule,
                            ordersModule,
                            orderDetailsModule,
                            createApiModule(provideApiConfig()),
                            createCheckoutModule(provideCheckoutConfig()),
                            aboutAppModule
                    )
            )
        }
    }
}
package com.punicapp.delivery.nextgen.domain


import android.location.Geocoder
import com.punicapp.delivery.nextgen.utils.FragmentResultHandler
import com.punicapp.delivery.nextgen.utils.GeocoderImpl
import com.punicapp.deliveryNext.domain.usecases.*
import com.punicapp.deliveryNext.domain.usecases.impl.*
import com.punicapp.deliveryNext.domain.utils.IGeocoder
import com.punicapp.deliveryNext.domain.utils.OrderValidator
import org.koin.core.qualifier.named
import org.koin.dsl.module

fun useCasesModule(orderValidator: OrderValidator) = module {
    single<IBasketUseCase> {
        BasketUseCaseImpl(
                basketRepo = get(),
                restaurantRepo = get(named("Restaurant"))
        )
    }

    single<IProductUseCase> {
        ProductsUseCaseImpl(
            productRepo = get(named("Product")),
            categoryRepo = get(named("ProductCategory"))
        )
    }

    single<IOrderUseCase> {
        OrderUseCaseImpl(
                keyValueRepository = get(),
                api = get(),
                basketCase = get(),
                checkoutConfig = get(),
                repoRestaurant = get(named("Restaurant")),
                orderValidator = orderValidator
        )
    }

    single<ICatalogUseCase> {
        CatalogUseCaseImpl(productsUseCase = get(), basketUseCase = get(), orderUseCase = get())
    }

    single<ISyncUseCase> {
        SyncUseCaseImpl(
            repoProducts = get(named("Product")),
            repoCategories = get(named("ProductCategory")),
            repoRestaurant = get(named("Restaurant")),
            api = get()
        )
    }

    single<IGeneralUseCase> {
        GeneralUseCaseImpl(api = get())
    }

    single<IGeocoder> {
        GeocoderImpl(Geocoder(get()))
    }

    single<IAddressMapUseCase> {
        AddressMapUseCaseImpl(get())
    }

    single {
        FragmentResultHandler()
    }
}
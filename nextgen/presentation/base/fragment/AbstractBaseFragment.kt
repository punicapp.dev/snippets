package com.punicapp.delivery.nextgen.presentation.base.fragment

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import com.punicapp.core.utils.KeyBoardUtils
import com.punicapp.delivery.nextgen.presentation.base.activity.AppActivity
import com.punicapp.delivery.nextgen.presentation.base.model.IToolbarCustomizer
import com.punicapp.delivery.nextgen.presentation.base.model.StubToolbarCustomizer
import com.punicapp.delivery.nextgen.presentation.base.viewmodel.BaseAppViewModel
import com.punicapp.mvvm.actions.UIActionConsumer
import com.punicapp.mvvm.android.AppViewModel
import com.punicapp.mvvm.android.BaseFragment
import com.punicapp.mvvm.android.IParentVMProvider
import org.koin.android.viewmodel.ext.android.getViewModel

abstract class AbstractBaseFragment<T : AppViewModel> : BaseFragment<T>(), IParentVMProvider {
    companion object {
        private val TAG: String = AbstractBaseFragment::class.java.simpleName
    }

    override fun onResume() {
        super.onResume()
        appActivity?.apply {
            invalidateToolbar(provideToolbarCustomizer())
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        hideKeyboardOnActivityCreated()
    }

    open fun hideKeyboardOnActivityCreated() {
        KeyBoardUtils.hideKeyboard(activity)
    }

    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<Boolean>(BaseAppViewModel.ACTION_PROGRESS_EVENT) { data ->
            showProgress(data)
        }.register<Throwable>(BaseAppViewModel.ACTION_ERROR_EVENT) { data ->
            showError(data)
        }
    }

    protected open fun showError(err: Throwable) {
        Toast.makeText(activity, err.toString(), Toast.LENGTH_SHORT).show()
        Log.d("Exception", "$err", err)
        // errorHandler.handleError(err)
    }

    val appActivity get() = activity as? AppActivity

    protected open fun showProgress(showProgress: Boolean) {
        appActivity?.showHideProgress(showProgress)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        rootView = null
    }


    override fun initViewModel(activity: FragmentActivity, clazz: Class<T>): T {
        return activity.getViewModel(clazz.kotlin)
    }

    override fun handleSubjecriptionError(throwable: Throwable?) {
        Log.d(TAG, "subscription error $throwable")
    }

    open fun provideToolbarCustomizer(): IToolbarCustomizer = StubToolbarCustomizer

    override fun provideVm(): AppViewModel = getAssociatedViewModel(activity)
}

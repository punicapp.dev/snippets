package com.punicapp.delivery.nextgen.presentation.base.activity

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.punicapp.delivery.nextgen.BR
import com.punicapp.delivery.nextgen.R
import com.punicapp.delivery.nextgen.data.payment.PaymentMakerImpl
import com.punicapp.delivery.nextgen.databinding.BaseAcBinding
import com.punicapp.delivery.nextgen.presentation.base.model.IToolbarCustomizer
import com.punicapp.delivery.nextgen.presentation.base.model.StubToolbarCustomizer
import com.punicapp.delivery.nextgen.presentation.base.viewmodel.ActivityViewModel
import com.punicapp.delivery.nextgen.presentation.cicerone.DefaultAppNavigator
import com.punicapp.delivery.nextgen.presentation.cicerone.SplashScreen
import com.punicapp.mvvm.actions.UIActionConsumer
import com.punicapp.mvvm.android.BaseActivity
import com.punicapp.mvvm.android.BaseFragment
import io.reactivex.functions.Consumer
import kotlinx.android.synthetic.main.base_ac.*
import kotlinx.android.synthetic.main.toolbar.view.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.getViewModel
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.yandex.money.android.sdk.Checkout


open class AppActivity : BaseActivity<BaseAcBinding>() {
    lateinit var viewModel: ActivityViewModel
    lateinit var activeTbCustomizer: IToolbarCustomizer

    private lateinit var toolbar: Toolbar

    private val navigatorHolder: NavigatorHolder by inject()
    private val router: Router by inject()
    private var navigator: Navigator? = null
    val paymentMaker by lazy { PaymentMakerImpl(this, this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
        navigator = createNavigator()
        configureStartScreen()
        initToolbar()
        setStatusBarColor()
    }

    private fun setStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val view = window.decorView
            view.systemUiVisibility = view.systemUiVisibility or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            window.statusBarColor = Color.WHITE
        }
    }

    private fun findCurrentFragment() =
            supportFragmentManager.findFragmentById(base_activity_container.id)

    private fun configureStartScreen() {
        router.replaceScreen(SplashScreen)
    }

    private fun initViewModel() {
        viewModel = createViewModel()
        lifecycle.addObserver(viewModel)

        val actionConsumer = UIActionConsumer()
        viewModel.listen(actionConsumer, Consumer { })
        binding.setVariable(BR.viewModel, viewModel)
    }

    private fun initToolbar() {
        val layout = LayoutInflater.from(this).inflate(R.layout.toolbar, toolbar_layout, true)
        toolbar = layout.toolbar
        setSupportActionBar(toolbar)

        val actionBar = supportActionBar ?: return
        actionBar.setDisplayShowTitleEnabled(false)
        actionBar.setDisplayShowCustomEnabled(true)
    }

    private fun createViewModel(): ActivityViewModel = getViewModel(ActivityViewModel::class)

    override fun onResume() {
        super.onResume()
        navigator?.let { navigatorHolder.setNavigator(it) }
        invalidateToolbar()
    }

    fun invalidateToolbar(customizer: IToolbarCustomizer = StubToolbarCustomizer) {
        activeTbCustomizer = customizer

        val actionBar = supportActionBar ?: return
        val settings = customizer.provideToolbarSettings()

        if (settings.toolbarVisible)
            actionBar.show()
        else {
            actionBar.hide()
            return
        }

        toolbar.visibility = View.VISIBLE
        actionBar.setHomeAsUpIndicator(ContextCompat.getDrawable(this, settings.iconId))

        actionBar.setDisplayShowHomeEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(settings.backButtonVisible)

        if (!settings.toolbarShadowVisible)
            toolbar.toolbar.background = null
        else
            toolbar.toolbar.setBackgroundResource(R.drawable.toolbar_bg)
        toolbar.toolbar_title.text = settings.toolbarText
    }

    override fun onPause() {
        super.onPause()
        navigator?.let { navigatorHolder.removeNavigator() }
    }

    override fun onDestroy() {
        super.onDestroy()
        fragment = null
    }

    private fun createNavigator(): Navigator? {
        val fragmentManager = supportFragmentManager
        val containerId = base_activity_container.id
        return DefaultAppNavigator(this, containerId, fragmentManager)
    }

    override fun getFragment(): BaseFragment<*>? = null

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        return if (id == android.R.id.home) {
            val consumed = activeTbCustomizer.onUpActionTap()
            if (!consumed) onBackPressed()
            true
        } else {
            super.onOptionsItemSelected(item)
        }
    }

    fun showHideProgress(show: Boolean) {
        viewModel.isBlockingProgressVisible.set(show)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            PaymentMakerImpl.REQUEST_CODE_TOKENIZE -> {
                val result = if (resultCode == Activity.RESULT_OK)
                    Checkout.createTokenizationResult(data ?: return)
                else
                    null
                paymentMaker.onPaymentResult(result?.paymentToken ?: "")
            }
            PaymentMakerImpl.REQUEST_CODE_SECURE -> {
                val error = when (resultCode) {
                    Activity.RESULT_OK -> {
                        null
                    }
                    else -> getString(R.string.payment_canceled)
                }
                paymentMaker.onSecureResult(error ?: "")
            }
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }
}
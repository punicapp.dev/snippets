package com.punicapp.delivery.nextgen.presentation.base.viewmodel

import android.app.Application
import androidx.databinding.ObservableBoolean

class ActivityViewModel(application: Application) : BaseAppViewModel(application) {
    companion object {
        const val ACTION_MENU = "ACTION_MENU"
    }

    val isBlockingProgressVisible = ObservableBoolean(false)


}
package com.punicapp.delivery.nextgen.presentation.base.viewmodel

import android.app.Application
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.OnLifecycleEvent
import com.punicapp.deliveryNext.domain.remote.AppResult
import com.punicapp.deliveryNext.domain.remote.AsyncAppResult
import com.punicapp.mvvm.actions.UIAction
import com.punicapp.mvvm.android.AppViewModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer

open class BaseAppViewModel(application: Application) : AppViewModel(application) {
    private var apiDisposable: CompositeDisposable? = null

    companion object {
        const val ACTION_PROGRESS_EVENT: String = "ACTION_PROGRESS_EVENT"
        const val ACTION_ERROR_EVENT: String = "ACTION_ERROR_EVENT"
    }

    protected open fun setProgressVisibility(isVisible: Boolean) {
        processor.onNext(UIAction(ACTION_PROGRESS_EVENT, isVisible))
    }

    protected open fun handleError(error: Throwable) {
        processor.onNext(UIAction(ACTION_ERROR_EVENT, error))
    }

    fun Disposable.addVMDisposable() = let {
        if (apiDisposable == null) {
            apiDisposable = CompositeDisposable()
        }
        apiDisposable?.add(it)
    }

    protected fun <T> AsyncAppResult<T>.appSubscribe(
        isShowProgress: Boolean = false,
        block: (T) -> Unit
    ) {
        if (isShowProgress) setProgressVisibility(true)
        subscribe { result ->
            if (isShowProgress) setProgressVisibility(false)
            when (result) {
                is AppResult.Ok -> block(result.data)
                is AppResult.Error -> handleError(result.error)
            }
        }
    }

    protected fun <T> Single<T>.appSubscribe(
        isShowProgress: Boolean = false,
        block: (T) -> Unit
    ) {
        this.observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { if (isShowProgress) setProgressVisibility(true) }
            .doFinally { if (isShowProgress) setProgressVisibility(false) }
            .subscribe(block, this@BaseAppViewModel::handleError)
            .addVMDisposable()
    }

    override fun doOnDestroy() {
        super.doOnDestroy()
        clearApiSubscriptions()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    override fun doOnPause() {
        super.doOnPause()
        clearApiSubscriptions()
    }

    private fun clearApiSubscriptions() {
        apiDisposable?.dispose()
        apiDisposable = null
    }

    public override fun listen(
        consumer: Consumer<UIAction>?,
        errorHandler: Consumer<in Throwable>
    ) {
        super.listen(consumer, errorHandler)
    }
}
package com.punicapp.delivery.nextgen.presentation.base.model

object StubToolbarCustomizer : IToolbarCustomizer {
    override fun onUpActionTap() = true

    override fun provideToolbarSettings() = ToolbarSettings()
}
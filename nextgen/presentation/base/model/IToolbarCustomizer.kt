package com.punicapp.delivery.nextgen.presentation.base.model

import com.punicapp.delivery.nextgen.presentation.base.model.ToolbarSettings

interface IToolbarCustomizer {
    fun onUpActionTap() : Boolean

    fun provideToolbarSettings() : ToolbarSettings

}
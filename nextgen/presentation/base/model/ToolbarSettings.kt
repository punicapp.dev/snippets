package com.punicapp.delivery.nextgen.presentation.base.model

import com.punicapp.delivery.nextgen.R

data class ToolbarSettings(
    val toolbarVisible: Boolean = false,
    val backButtonVisible: Boolean = true,
    val toolbarShadowVisible: Boolean = true,
    val iconId: Int = R.drawable.ic_back_arrow,
    val toolbarText: String = ""
)
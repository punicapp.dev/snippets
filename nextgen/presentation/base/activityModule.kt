package com.punicapp.delivery.nextgen.presentation.base

import com.punicapp.delivery.nextgen.presentation.base.viewmodel.ActivityViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val activityModule = module {
    viewModel { ActivityViewModel(application = get()) }
}
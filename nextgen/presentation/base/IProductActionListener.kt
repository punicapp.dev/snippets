package com.punicapp.delivery.nextgen.presentation.base

import com.punicapp.deliveryNext.domain.entities.BasketProduct

interface IProductActionListener {
    fun onLongClickCartItem(product: BasketProduct)
    fun onRemoveItem(product: BasketProduct)
    fun onDecreaseItemCount(product: BasketProduct)
    fun onIncreaseItemCount(product: BasketProduct)
}
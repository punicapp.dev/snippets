package com.punicapp.delivery.nextgen.presentation.base.dialogs

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.punicapp.delivery.nextgen.BR
import com.punicapp.delivery.nextgen.R
import com.punicapp.delivery.nextgen.presentation.base.viewmodel.BaseAppViewModel
import com.punicapp.mvvm.android.IParentVMProvider
import com.punicapp.mvvm.dialogs.alert.AlertAction
import com.punicapp.mvvm.dialogs.alert.AlertDialogContent

object DialogProvider {
    fun getMessageDialog(context: Context, messageRes: Int, vmProvider: IParentVMProvider, actionId: String = AlertAction.POSITIVE.name) = context.run {
        AlertDialogContent.Builder(vmProvider)
                .message(getString(messageRes))
                .header(getString(R.string.attention))
                .okLabel(getString(R.string.ok), actionId)
    }

    fun getYesNoDialog(context: Context, messageRes: Int, okMessageRes: Int, noMessageRes: Int,
                       vmProvider: IParentVMProvider,
                       actionPositiveId: String = AlertAction.POSITIVE.name,
                       actionNegativeId: String = AlertAction.NEGATIVE.name) = context.run {
        AlertDialogContent.Builder(vmProvider)
                .message(getString(messageRes))
                .okLabel(getString(okMessageRes), actionPositiveId)
                .noLabel(getString(noMessageRes), actionNegativeId)
    }

    fun getErrorDialog(context: Context, message: String, vmProvider: IParentVMProvider, actionId: String = AlertAction.POSITIVE.name) = context.run {
        AlertDialogContent.Builder(vmProvider)
                .message(message)
                .header(getString(R.string.attention))
                .okLabel(getString(R.string.ok), actionId)
    }

    fun getBottomSheetProductDetailsDialog(
            context: Context,
            viewModel: BaseAppViewModel
    ): BottomSheetDialog {
        return BottomSheetDialog(context).apply {
            val inflater = LayoutInflater.from(context)
            val binding = DataBindingUtil.inflate<ViewDataBinding>(inflater, R.layout.bottom_sheet_product_details, null, false)
            binding.setVariable(BR.viewModel, viewModel)
            binding.executePendingBindings()
            setContentView(binding.root)

            (findViewById(com.google.android.material.R.id.design_bottom_sheet) as? FrameLayout)?.let {
                val behavior = BottomSheetBehavior.from(it)
                behavior.state = BottomSheetBehavior.STATE_EXPANDED
                behavior.peekHeight = 0
                behavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
                    override fun onStateChanged(view: View, state: Int) {
                        if (state == BottomSheetBehavior.STATE_COLLAPSED) {
                            dismiss()
                        }
                    }

                    override fun onSlide(var1: View, var2: Float) {

                    }
                })
            }
        }
    }
}
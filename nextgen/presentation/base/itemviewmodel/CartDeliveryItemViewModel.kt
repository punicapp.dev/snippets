package com.punicapp.delivery.nextgen.presentation.base.itemviewmodel

import androidx.databinding.ObservableField
import com.punicapp.delivery.nextgen.presentation.base.IProductActionListener
import com.punicapp.delivery.nextgen.presentation.cart.CartViewModel
import com.punicapp.delivery.nextgen.presentation.order_details.OrderDetailsViewModel
import com.punicapp.delivery.nextgen.utils.toPriceDisplayFormatWithoutSpace
import com.punicapp.deliveryNext.domain.entities.BasketItem
import com.punicapp.deliveryNext.domain.entities.BasketProduct
import com.punicapp.deliveryNext.domain.entities.DeliveryItem
import com.punicapp.mvvm.adapters.AdapterItemViewModel
import com.punicapp.mvvm.android.AppViewModel


open class DeliveryItemViewModel<T : AppViewModel> : AdapterItemViewModel<DeliveryItem, T>() {
    val productPrice = ObservableField<String>()

    lateinit var product: BasketProduct

    override fun bindData(data: DeliveryItem) {
        productPrice.set(data.singlePrice.toPriceDisplayFormatWithoutSpace())
    }
}

class CartDeliveryItemViewModel : DeliveryItemViewModel<CartViewModel>()
class OrderDeliveryItemViewModel : DeliveryItemViewModel<OrderDetailsViewModel>()
package com.punicapp.delivery.nextgen.presentation.base.itemviewmodel

import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import androidx.core.content.ContextCompat
import androidx.databinding.ObservableField
import com.punicapp.delivery.nextgen.R
import com.punicapp.delivery.nextgen.presentation.base.IProductActionListener
import com.punicapp.delivery.nextgen.presentation.cart.CartViewModel
import com.punicapp.delivery.nextgen.presentation.order_details.OrderDetailsViewModel
import com.punicapp.delivery.nextgen.utils.toPriceDisplayFormat
import com.punicapp.delivery.nextgen.utils.toPriceDisplayFormatWithoutSpace
import com.punicapp.delivery.nextgen.utils.toProductCountFormat
import com.punicapp.deliveryNext.domain.entities.Addon
import com.punicapp.deliveryNext.domain.entities.BasketProduct
import com.punicapp.deliveryNext.domain.entities.toDisplayFormat
import com.punicapp.mvvm.adapters.AdapterItemViewModel
import com.punicapp.mvvm.android.AppViewModel


open class ProductItemViewModel<T> : AdapterItemViewModel<BasketProduct, T>() where T : AppViewModel, T : IProductActionListener {
    val productName = ObservableField<CharSequence>()
    val productCount = ObservableField<String>()
    val productPrice = ObservableField<String>()
    var addonsString = ObservableField<String>()
    var addonsPricesString = ObservableField<String>()

    lateinit var product: BasketProduct

    override fun bindData(data: BasketProduct) {
        product = data
        val name = SpannableStringBuilder(data.product.name)
        val description = getAddonsString(data.selectedAddons, data.quantity)
        addonsString.set(description.first)
        addonsPricesString.set(description.second)
        name.append(" ${data.product.measure.toDisplayFormat()}")
        name.setSpan(ForegroundColorSpan(ContextCompat.getColor(parent.getApplication(), R.color.subtitle_text_c)), data.product.name.length, name.length, Spannable.SPAN_EXCLUSIVE_INCLUSIVE)
        productCount.set(data.quantity.toProductCountFormat())
        productName.set(name)
        productPrice.set(data.singlePrice.toPriceDisplayFormatWithoutSpace())
    }

    private fun getAddonsString(selectedAddons: Map<Long?, MutableList<Addon?>>?, quantity: Int): Pair<String?, String?> {
        if (!selectedAddons.isNullOrEmpty()) {
            var addons = ""
            var prices = ""
            selectedAddons.forEach { entry ->
                entry.value.forEach { addon ->
                    addons += "${addon?.name}${System.lineSeparator()}"
                    prices += "${if (quantity > 1) "${quantity}x" else ""}${addon?.price?.toPriceDisplayFormat()}${System.lineSeparator()}"
                }
            }
            addons = addons.trim()
            prices = prices.trim()
            return addons to prices
        }
        return null to null
    }

    fun onLongClick() {
        parent.onLongClickCartItem(product)
    }
}

class CartItemViewModel : ProductItemViewModel<CartViewModel>() {
    fun onRemoveClick() {
        parent.onRemoveItem(product)
    }

    fun onDecreaseCountClick() {
        parent.onDecreaseItemCount(product)
    }

    fun onIncreaseCountClick() {
        parent.onIncreaseItemCount(product)
    }
}

class OrderProductItemViewModel : ProductItemViewModel<OrderDetailsViewModel>()
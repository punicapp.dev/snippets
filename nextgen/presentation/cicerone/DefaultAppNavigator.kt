package com.punicapp.delivery.nextgen.presentation.cicerone

import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import ru.terrakok.cicerone.android.support.SupportAppNavigator

class DefaultAppNavigator(
        activity: FragmentActivity,
        containerId: Int,
        fragmentManager: FragmentManager = activity.supportFragmentManager
) : SupportAppNavigator(activity, fragmentManager, containerId)
package com.punicapp.delivery.nextgen.presentation.cicerone

import org.koin.dsl.module
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router

typealias AppCicerone = Cicerone<Router>
val ciceroneModule = module {
    single { AppCicerone.create() }
    single<Router> { get<AppCicerone>().router }
    single<NavigatorHolder> { get<AppCicerone>().navigatorHolder }
}
package com.punicapp.delivery.nextgen.presentation.cicerone

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.punicapp.delivery.nextgen.presentation.about.AboutAppFragment
import com.punicapp.delivery.nextgen.presentation.address.AddressMapFragment
import com.punicapp.delivery.nextgen.presentation.base.fragment.AbstractBaseFragment
import com.punicapp.delivery.nextgen.presentation.cart.CartFragment
import com.punicapp.delivery.nextgen.presentation.menu.MenuFragment
import com.punicapp.delivery.nextgen.presentation.order.OrderCheckoutFragment
import com.punicapp.delivery.nextgen.presentation.order_details.OrderDetailsFragment
import com.punicapp.delivery.nextgen.presentation.orders.OrdersFragment
import com.punicapp.delivery.nextgen.presentation.splash.SplashFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

sealed class Screen(
    private val clazz: Class<out AbstractBaseFragment<*>>,
    val args: Bundle? = null
) : SupportAppScreen() {

    override fun getFragment(): Fragment {
        val fragment = clazz.newInstance()
        fragment.arguments = args
        return fragment
    }
}

object SplashScreen : Screen(SplashFragment::class.java)

object OrderCheckoutScreen : Screen(OrderCheckoutFragment::class.java)
object MenuScreen : Screen(MenuFragment::class.java)
object CartScreen : Screen(CartFragment::class.java)
object AddressMapScreen : Screen(AddressMapFragment::class.java)
object OrdersScreen : Screen(OrdersFragment::class.java)
object AboutAppScreen : Screen(AboutAppFragment::class.java)
class OrdersDetailsScreen(args: Bundle?) : Screen(OrderDetailsFragment::class.java, args)

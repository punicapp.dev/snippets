package com.punicapp.delivery.nextgen.presentation.cart

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.punicapp.delivery.nextgen.R
import com.punicapp.delivery.nextgen.databinding.CartFrBinding
import com.punicapp.deliveryNext.domain.usecases.IBasketUseCase
import com.punicapp.delivery.nextgen.presentation.base.dialogs.DialogProvider
import com.punicapp.delivery.nextgen.presentation.base.fragment.AbstractBaseFragment
import com.punicapp.delivery.nextgen.presentation.base.model.IToolbarCustomizer
import com.punicapp.delivery.nextgen.presentation.base.model.ToolbarSettings
import com.punicapp.delivery.nextgen.presentation.cicerone.OrderCheckoutScreen
import com.punicapp.mvvm.actions.UIAction
import com.punicapp.mvvm.actions.UIActionConsumer
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router

class CartFragment : AbstractBaseFragment<CartViewModel>() {

    private val useCase: IBasketUseCase by inject()
    private val router: Router by inject()

    override fun initBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        appViewModel: CartViewModel
    ): View {
        val binding = CartFrBinding.inflate(inflater, container, false)
        binding.viewModel = appViewModel
        binding.executePendingBindings()
        appViewModel.init(useCase)

        return binding.root
    }


    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<UIAction>(CartViewModel.CHECKOUT) {
            router.navigateTo(OrderCheckoutScreen)
        }.register<Unit>(CartViewModel.SHOW_POPUP) {
            DialogProvider.getYesNoDialog(
                    activity ?: return@register,
                    R.string.delete_cart_item_confirmation,
                    R.string.yes,
                    R.string.no,
                    this,
                    CartViewModel.ACTION_CONFIRMED
            ).show(fragmentManager ?: return@register)
        }.register<Unit>(CartViewModel.ACTION_RETURN_TO_CATALOG) {
            router.exit()
        }
    }

    override fun provideToolbarCustomizer(): IToolbarCustomizer = object : IToolbarCustomizer {
        override fun onUpActionTap(): Boolean = false

        override fun provideToolbarSettings(): ToolbarSettings = ToolbarSettings(
            toolbarVisible = true,
            backButtonVisible = true,
            toolbarShadowVisible = true,
            toolbarText = getString(R.string.cart_title)
        )
    }
}
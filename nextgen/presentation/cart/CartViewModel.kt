package com.punicapp.delivery.nextgen.presentation.cart

import android.app.Application
import androidx.databinding.ObservableField
import com.punicapp.delivery.nextgen.BR
import com.punicapp.delivery.nextgen.R
import com.punicapp.delivery.nextgen.presentation.base.IProductActionListener
import com.punicapp.delivery.nextgen.presentation.base.itemviewmodel.CartDeliveryItemViewModel
import com.punicapp.delivery.nextgen.presentation.base.itemviewmodel.CartItemViewModel
import com.punicapp.delivery.nextgen.presentation.base.viewmodel.BaseAppViewModel
import com.punicapp.delivery.nextgen.utils.toPriceDisplayFormat
import com.punicapp.deliveryNext.domain.entities.Basket
import com.punicapp.deliveryNext.domain.entities.BasketItem
import com.punicapp.deliveryNext.domain.entities.BasketProduct
import com.punicapp.deliveryNext.domain.entities.DeliveryItem
import com.punicapp.deliveryNext.domain.usecases.IBasketUseCase
import com.punicapp.mvvm.actions.UIAction
import com.punicapp.mvvm.adapters.DiffUtilsDataProcessor
import com.punicapp.mvvm.adapters.VmAdapter
import com.punicapp.mvvm.adapters.VmAdapterItem
import io.reactivex.Observer

class CartViewModel(application: Application) : BaseAppViewModel(application), IProductActionListener {
    companion object {
        const val ITEM_BASKET_PRODUCT = 1
        const val ITEM_BASKET_DELIVERY = 2

        const val CHECKOUT = "checkout"
        const val SHOW_POPUP = "show_popup"

        const val ACTION_CONFIRMED = "action_confirmed"
        const val ACTION_RETURN_TO_CATALOG = "action_return_to_catalog"
    }

    private lateinit var observer: Observer<List<VmAdapterItem>>
    lateinit var useCase: IBasketUseCase
    lateinit var productToDelete: BasketProduct
    var data = mutableListOf<VmAdapterItem>()
    val orderPrice = ObservableField<String>()
    val deliveryTime = ObservableField<String>()

    fun init(useCase: IBasketUseCase) {
        this.useCase = useCase
        loadCart()
    }

    private fun loadCart() {
        useCase.getBasket().appSubscribe {
            showBasketProducts(it)
        }

        useCase.getDeliveryTime().appSubscribe {
            deliveryTime.set(it)
        }
    }

    private fun showBasketProducts(basket: Basket) {
        orderPrice.set(basket.orderSum.toPriceDisplayFormat())
        data = basket.products.map {
            when (it) {
                is DeliveryItem -> VmAdapterItem(it, ITEM_BASKET_DELIVERY)
                is BasketProduct -> VmAdapterItem(it, ITEM_BASKET_PRODUCT)
            }
        }.toMutableList()
        observer.onNext(data)

        if (data.isEmpty())
            processor.onNext(UIAction(ACTION_RETURN_TO_CATALOG))
    }

    override fun onLongClickCartItem(product: BasketProduct) {
        productToDelete = product
        processor.onNext(UIAction(SHOW_POPUP))
    }

    override fun onRemoveItem(product: BasketProduct) {
        productToDelete = product
        processor.onNext(UIAction(SHOW_POPUP))
    }

    override fun onDecreaseItemCount(product: BasketProduct) {
        useCase.decreaseCount(product).appSubscribe {
            showBasketProducts(it)
        }
    }

    override fun onIncreaseItemCount(product: BasketProduct) {
        useCase.increaseCount(product).appSubscribe {
            showBasketProducts(it)
        }
    }

    fun getAdapter(): VmAdapter = VmAdapter.Builder(this, BR.viewModel)
            .defaultProducer(
                    ITEM_BASKET_PRODUCT,
                    R.layout.cart_item,
                    BasketProduct::class.java,
                    CartItemViewModel::class.java
            ).defaultProducer(
                    ITEM_BASKET_DELIVERY,
                    R.layout.cart_delivery_item,
                    DeliveryItem::class.java,
                    CartDeliveryItemViewModel::class.java
            ).build()
            .also {
                observer = it.dataWithTypeReceiver
            }

    fun checkout() {
        processor.onNext(UIAction(CHECKOUT))
    }

    override fun onActionFromChild(action: UIAction?) {
        if (action?.actionId == ACTION_CONFIRMED) {
            useCase.deleteProduct(productToDelete.product, productToDelete.selectedAddons)
                    .appSubscribe {
                        showBasketProducts(it)
                    }
        }
    }
}
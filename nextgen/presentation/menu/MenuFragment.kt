package com.punicapp.delivery.nextgen.presentation.menu

import android.app.Dialog
import android.view.*
import com.punicapp.delivery.nextgen.R
import com.punicapp.delivery.nextgen.databinding.MenuFrBinding
import com.punicapp.delivery.nextgen.presentation.base.dialogs.DialogProvider
import com.punicapp.delivery.nextgen.presentation.base.fragment.AbstractBaseFragment
import com.punicapp.delivery.nextgen.presentation.base.model.IToolbarCustomizer
import com.punicapp.delivery.nextgen.presentation.base.model.ToolbarSettings
import com.punicapp.delivery.nextgen.presentation.cicerone.AboutAppScreen
import com.punicapp.delivery.nextgen.presentation.cicerone.CartScreen
import com.punicapp.delivery.nextgen.presentation.cicerone.OrdersScreen
import com.punicapp.mvvm.actions.UIActionConsumer
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router


class MenuFragment : AbstractBaseFragment<MenuViewModel>() {
    private val router: Router by inject()
    private var ordersMenuItem: MenuItem? = null
    private var dialog: Dialog? = null

    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<Unit>(MenuViewModel.ACTION_PRODUCT_POPUP) {
            openProductDetails()
        }.register<Unit>(MenuViewModel.ACTION_CLOSE_POPUP) {
            dialog?.dismiss()
        }.register<Unit>(MenuViewModel.ACTION_PRODUCT_ADDED) {
            DialogProvider.getMessageDialog(activity ?: return@register, R.string.message_product_added, this)
                    .show(fragmentManager ?: return@register)
        }.register<Unit>(MenuViewModel.ACTION_TO_ORDER) {
            router.navigateTo(CartScreen)
        }.register<Boolean>(MenuViewModel.ACTION_ORDERS) {
            ordersMenuItem?.isVisible = it
        }
    }

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?, appViewModel: MenuViewModel): View {
        val binding = MenuFrBinding.inflate(inflater, container, false)
        binding.viewModel = appViewModel
        binding.executePendingBindings()
        appViewModel.init()

        return binding.root
    }

    override fun provideToolbarCustomizer(): IToolbarCustomizer = object : IToolbarCustomizer {
        override fun onUpActionTap(): Boolean {
            router.navigateTo(AboutAppScreen)
            return true
        }

        override fun provideToolbarSettings(): ToolbarSettings = ToolbarSettings(
                toolbarVisible = true,
                backButtonVisible = true,
                iconId = R.drawable.ic_info,
                toolbarShadowVisible = false,
                toolbarText = getString(R.string.menu_title)
        )
    }

    private fun openProductDetails() {
        dialog = DialogProvider.getBottomSheetProductDetailsDialog(
                requireContext(),
                appViewModel
        ).also { dialog ->
            dialog.show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.order_menu, menu)
        ordersMenuItem = menu.findItem(R.id.menu_orders)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onResume() {
        super.onResume()
        setHasOptionsMenu(true)
    }

    override fun onPause() {
        super.onPause()
        setHasOptionsMenu(false)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_orders -> {
                router.navigateTo(OrdersScreen)
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
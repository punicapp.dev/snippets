package com.punicapp.delivery.nextgen.presentation.menu

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val menuModule = module {
    viewModel { MenuViewModel(application = get(), useCase = get()) }
}
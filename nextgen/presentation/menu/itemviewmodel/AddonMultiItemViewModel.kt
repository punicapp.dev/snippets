package com.punicapp.delivery.nextgen.presentation.menu.itemviewmodel

import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import com.punicapp.delivery.nextgen.presentation.menu.MenuViewModel
import com.punicapp.deliveryNext.domain.entities.Addon
import com.punicapp.deliveryNext.domain.entities.AddonCategory
import com.punicapp.mvvm.adapters.AdapterItemViewModel

class AddonMultiItemViewModel : AdapterItemViewModel<AddonCategory, MenuViewModel>() {
    val title = ObservableField<String>()
    val options = ObservableArrayList<Addon>()

    lateinit var data: AddonCategory

    override fun bindData(data: AddonCategory) {
        this.data = data
        title.set(data.name)
        options.clear()
        options.addAll(data.addons ?: emptyList())
    }

    fun onAddonSelected(addon: Addon, selected: Boolean) {
        parent.editAddon(addon, data, selected)
    }
}
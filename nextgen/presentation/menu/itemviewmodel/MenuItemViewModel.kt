package com.punicapp.delivery.nextgen.presentation.menu.itemviewmodel

import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import com.punicapp.delivery.nextgen.presentation.menu.MenuViewModel
import com.punicapp.delivery.nextgen.utils.toPriceDisplayFormat
import com.punicapp.deliveryNext.domain.entities.Product
import com.punicapp.deliveryNext.domain.entities.toDisplayFormat
import com.punicapp.mvvm.adapters.AdapterItemViewModel

class MenuItemViewModel : AdapterItemViewModel<Product, MenuViewModel>() {
    val image = ObservableField<String>()
    val name = ObservableField<String>()
    val price = ObservableField<String>()
    val measure = ObservableField<String>()
    val inBasket = ObservableInt()

    lateinit var data: Product

    override fun bindData(data: Product) {
        this.data = data
        image.set(data.image)
        name.set(data.name)
        price.set(data.price.toPriceDisplayFormat())
        measure.set(data.measure.toDisplayFormat())
        inBasket.set(parent.basket.get()?.findProductById(data.id)?.quantity ?: 0)
    }

    fun onClick() {
        parent.onProductClick(data)
    }
}
package com.punicapp.delivery.nextgen.presentation.menu.itemviewmodel

import androidx.databinding.ObservableField
import com.punicapp.delivery.nextgen.presentation.menu.MenuViewModel
import com.punicapp.deliveryNext.domain.entities.ProductCategory
import com.punicapp.mvvm.adapters.AdapterItemViewModel

class MenuSectionItemViewModel : AdapterItemViewModel<ProductCategory, MenuViewModel>() {
    val name = ObservableField<String>()

    lateinit var data: ProductCategory

    override fun bindData(data: ProductCategory) {
        this.data = data
        name.set(data.name)
    }
}
package com.punicapp.delivery.nextgen.presentation.menu

import android.app.Application
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.tabs.TabLayout
import com.punicapp.delivery.nextgen.BR
import com.punicapp.delivery.nextgen.R
import com.punicapp.delivery.nextgen.presentation.base.viewmodel.BaseAppViewModel
import com.punicapp.delivery.nextgen.presentation.menu.itemviewmodel.AddonMultiItemViewModel
import com.punicapp.delivery.nextgen.presentation.menu.itemviewmodel.AddonSingleItemViewModel
import com.punicapp.delivery.nextgen.presentation.menu.itemviewmodel.MenuItemViewModel
import com.punicapp.delivery.nextgen.presentation.menu.itemviewmodel.MenuSectionItemViewModel
import com.punicapp.delivery.nextgen.utils.onChanged
import com.punicapp.delivery.nextgen.utils.toPriceDisplayFormat
import com.punicapp.deliveryNext.domain.entities.*
import com.punicapp.deliveryNext.domain.usecases.ICatalogUseCase
import com.punicapp.mvvm.actions.UIAction
import com.punicapp.mvvm.adapters.DiffUtilsDataProcessor
import com.punicapp.mvvm.adapters.VmAdapter
import com.punicapp.mvvm.adapters.VmAdapterItem
import com.punicapp.mvvm.dialogs.alert.AlertAction
import io.reactivex.Observer

class MenuViewModel(application: Application, private val useCase: ICatalogUseCase) : BaseAppViewModel(application) {
    companion object {
        const val ACTION_PRODUCT_POPUP = "ACTION_PRODUCT_POPUP"
        const val ACTION_PRODUCT_ADDED = "ACTION_PRODUCT_ADDED"
        const val ACTION_CLOSE_POPUP = "ACTION_CLOSE_POPUP"
        const val ACTION_ORDERS = "ACTION_ORDERS"
        const val ACTION_TO_ORDER = "action_to_order"

        const val SECTION_HEADER = 1

        const val MENU_ITEM = 2
        private const val ADDON_SINGLE_ITEM = 1

        private const val ADDON_MULTI_ITEM = 2
    }

    val tabs = ObservableArrayList<ProductCategory>()
    val data = mutableListOf<VmAdapterItem>()
    val cartItemCount = ObservableInt()
    val cartSum = ObservableField<String>()
    val deliveryTime = ObservableField<String>()

    private var isScrolling: Boolean = false

    val basket = ObservableField<Basket>().onChanged { invalidateBasket(it) }

    var product: Product? = null
    val count = ObservableInt(1)

    private lateinit var observer: Observer<List<VmAdapterItem>>
    private lateinit var addonObserver: Observer<List<VmAdapterItem>>
    private lateinit var invalidator: Observer<List<Int>>

    fun init() {
        loadMenu()
        checkBasket()
    }

    private fun loadMenu() {
        useCase.getCategories().appSubscribe {
            fillCategories(it)
        }

        useCase.getDeliveryTime().appSubscribe {
            deliveryTime.set(it)
        }
    }

    private fun fillCategories(categories: List<ProductCategory>) {
        data.clear()
        tabs.clear()
        categories.forEach { category ->
            useCase.getProductsByCategory(category.id)
                    .appSubscribe {
                        addSection(category, it)
                    }
        }
    }

    private fun addSection(category: ProductCategory, items: List<Product>) {
        if (items.isEmpty())
            return

        if (tabs.contains(category))
            return

        tabs.add(category)
        data.add(VmAdapterItem(category, SECTION_HEADER))
        data.addAll(items.map { VmAdapterItem(it, MENU_ITEM) })
        observer.onNext(data)
    }

    fun onToOrderClick() {
        processor.onNext(UIAction(ACTION_TO_ORDER))
    }

    fun onProductClick(product: Product) {
        this.product = product
        count.set(useCase.resetCounter())
        useCase.resetAddons()
        processor.onNext(UIAction(ACTION_PRODUCT_POPUP))
    }

    fun onMinusClick() {
        count.set(useCase.decreaseCounter())
    }

    fun onPlusClick() {
        count.set(useCase.increaseCounter())
    }

    fun onAddToCart() {
        product?.let { product ->
            useCase.editBasket(product, false)
                    .appSubscribe { basket ->
                        processor.onNext(UIAction(ACTION_PRODUCT_ADDED))
                        this.basket.set(basket)
                    }
        }
    }

    private fun checkBasket() {
        useCase.getBasket()
                .appSubscribe { basket ->
                    this.basket.set(basket)
                }
    }

    private fun invalidateBasket(basket: Basket?) {
        if (basket == null)
            return

        cartItemCount.set(basket.productCount)
        cartSum.set(basket.orderSum.toPriceDisplayFormat())
        invalidator.onNext((0..data.size).toList())
    }

    fun getAdapter(): VmAdapter = VmAdapter.Builder(this, BR.viewModel)
            .defaultProducer(
                    SECTION_HEADER,
                    R.layout.menu_section_item,
                    ProductCategory::class.java,
                    MenuSectionItemViewModel::class.java
            )
            .defaultProducer(
                    MENU_ITEM,
                    R.layout.menu_item,
                    Product::class.java,
                    MenuItemViewModel::class.java
            )
            .processor(DiffUtilsDataProcessor())
            .build()
            .also {
                observer = it.dataWithTypeReceiver
                invalidator = it.dataInvalidator
            }

    fun getSpanSizeLookup(view: RecyclerView): GridLayoutManager.SpanSizeLookup {
        return object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return when (view.adapter?.getItemViewType(position)) {
                    SECTION_HEADER -> 2
                    else -> 1
                }
            }
        }
    }

    fun getTabSelectedListener(view: RecyclerView): TabLayout.OnTabSelectedListener {
        val smoothScroller = object : LinearSmoothScroller(view.context) {
            override fun getVerticalSnapPreference(): Int = SNAP_TO_START
        }

        fun scrollToSection(tab: TabLayout.Tab) {
            if (!isScrolling) {
                val position =
                        data.indexOfFirst { it.itemType == SECTION_HEADER && (it.`object` as ProductCategory).name == tab.text }
                if (position < 0)
                    return
                smoothScroller.targetPosition = position
                (view.layoutManager as GridLayoutManager).startSmoothScroll(smoothScroller)
            }
        }

        return object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab) {
                scrollToSection(tab)
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabSelected(tab: TabLayout.Tab) {
                scrollToSection(tab)
            }
        }
    }

    fun getRecyclerScrollListener(view: TabLayout): RecyclerView.OnScrollListener {
        return object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (isScrolling) {
                    val itemPosition =
                            (recyclerView.layoutManager as GridLayoutManager).findFirstCompletelyVisibleItemPosition()
                    val tabPosition = data.subList(
                            0,
                            itemPosition + 1
                    ).filter { it.itemType == SECTION_HEADER }.size - 1
                    val tab = view.getTabAt(tabPosition)
                    tab?.takeIf { !it.isSelected }?.select()
                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                    isScrolling = true
                } else if (newState == RecyclerView.SCROLL_STATE_IDLE)
                    isScrolling = false
            }
        }
    }

    fun formattedProductPrice() = product?.price?.toPriceDisplayFormat() ?: ""

    override fun onActionFromChild(action: UIAction?) {
        if (action?.actionId == AlertAction.POSITIVE.name) {
            processor.onNext(UIAction(ACTION_CLOSE_POPUP))
        }
        useCase.resetAddons()
    }

    fun getAddonAdapter(): VmAdapter = VmAdapter.Builder(this, BR.viewModel)
            .defaultProducer(ADDON_SINGLE_ITEM, R.layout.addon_single_item, AddonCategory::class.java, AddonSingleItemViewModel::class.java)
            .defaultProducer(ADDON_MULTI_ITEM, R.layout.addon_multi_item, AddonCategory::class.java, AddonMultiItemViewModel::class.java)
            .processor(DiffUtilsDataProcessor())
            .build()
            .also {
                addonObserver = it.dataWithTypeReceiver
                product?.addonCategories
                        ?.filter { it.addons?.isNullOrEmpty() == false }
                        ?.mapNotNull { category ->
                            return@mapNotNull when (category.type) {
                                AddonCategoryType.SINGLE -> VmAdapterItem(category, ADDON_SINGLE_ITEM)
                                AddonCategoryType.MULTI -> VmAdapterItem(category, ADDON_MULTI_ITEM)
                                else -> null
                            }
                        }?.let { addonObserver.onNext(it) }
            }

    fun editAddon(addon: Addon, data: AddonCategory, selected: Boolean) {
        useCase.editAddon(addon, data, selected)
    }

    override fun doOnResume() {
        super.doOnResume()
        useCase.getOrders().appSubscribe { data ->
            processor.onNext(UIAction(ACTION_ORDERS, data.isNotEmpty()))
        }
    }
}
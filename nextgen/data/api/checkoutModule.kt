package com.punicapp.delivery.nextgen.data.api

import com.punicapp.deliveryNext.data.api.CheckoutConfig
import org.koin.dsl.module

fun createCheckoutModule(config: CheckoutConfig) = module {
    single<CheckoutConfig> { config }
}
package com.punicapp.delivery.nextgen.data.api

import android.util.Log
import com.punicapp.deliveryNext.data.api.ApiConfig
import com.punicapp.deliveryNext.data.api.DeliveryApi
import com.punicapp.deliveryNext.data.api.IApiService
import org.koin.dsl.module

fun createApiModule(config: ApiConfig) = module {
    single<IApiService> {
        DeliveryApi(config) { str: String ->
            str.chunked(4000).forEach {
                Log.d("HttpClient", it)
            }
        }
    }
}
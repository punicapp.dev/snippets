package com.punicapp.delivery.nextgen.data.repo

import android.content.Context
import android.content.SharedPreferences
import com.punicapp.deliveryNext.data.storage.IKeyValueRepository
import com.punicapp.deliveryNext.data.storage.IRepository
import com.punicapp.deliveryNext.data.storage.impl.InMemoryRepository
import com.punicapp.deliveryNext.domain.entities.ProductCategory
import com.punicapp.deliveryNext.domain.entities.Product
import com.punicapp.deliveryNext.domain.entities.Restaurant
import org.koin.core.qualifier.named
import org.koin.dsl.module

const val PREFS_NAME = "prefs"

val repositoryModule = module {
    single<SharedPreferences> { get<Context>().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE) }
    single<IKeyValueRepository> { SharedPreferencesKeyValueRepositoryDelegate(get()) }

    single<IRepository<Product>> (qualifier = named("Product")){ InMemoryRepository()}
    single<IRepository<ProductCategory>> (qualifier = named("ProductCategory")) { InMemoryRepository()}
    single<IRepository<Restaurant>> (qualifier = named("Restaurant")) { InMemoryRepository()}
}
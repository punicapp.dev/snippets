package com.punicapp.delivery.nextgen.data.repo

import android.content.SharedPreferences
import com.punicapp.deliveryNext.data.storage.IKeyValueRepository

class SharedPreferencesKeyValueRepositoryDelegate(private val sharedPreferences: SharedPreferences) : IKeyValueRepository {
    override fun getBoolean(key: String, defaultValue: Boolean): Boolean =
            sharedPreferences.getBoolean(key, defaultValue)

    override fun getString(key: String, defaultValue: String?): String? =
            sharedPreferences.getString(key, defaultValue)

    override fun getInt(key: String, defaultValue: Int): Int =
            sharedPreferences.getInt(key, defaultValue)

    override fun putBoolean(key: String, value: Boolean) {
        sharedPreferences.edit().putBoolean(key, value).apply()
    }

    override fun putString(key: String, value: String) {
        sharedPreferences.edit().putString(key, value).apply()
    }

    override fun putInt(key: String, value: Int) {
        sharedPreferences.edit().putInt(key, value).apply()
    }

    override fun remove(key: String) {
        sharedPreferences.edit().remove(key).apply()
    }
}
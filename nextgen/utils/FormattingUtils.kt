package com.punicapp.delivery.nextgen.utils

import com.punicapp.delivery.nextgen.DeliveryApp
import com.punicapp.delivery.nextgen.R
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

fun Double.toPriceDisplayFormat(): String {
    return DeliveryApp.context.resources.getString(R.string.ruble, DecimalFormat("#.##").format(this))
}

fun Double.toPriceDisplayFormatWithoutSpace(): String {
    return DeliveryApp.context.resources.getString(R.string.ruble_without_space, DecimalFormat("#.##").format(this))
}

fun Double.toPriceDisplayFormatWithPlus(): String {
    return DeliveryApp.context.resources.getString(R.string.ruble_with_plus, DecimalFormat("#.##").format(this))
}

fun Int.toProductCountFormat(): String {
    return DeliveryApp.context.resources.getString(R.string.x_count, this.toString())
}

val dateFormatDDMMYYYY = "dd.MM.yyyy"
val dateFormatHHMMDDMMYYYY = "HH:mm dd.MM.yyyy"

fun String.formatDate(format: String): String? {
    val sourceFormatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
    val formatter = SimpleDateFormat(format, Locale.getDefault())
    return formatter.format(sourceFormatter.parse(this))
}
package com.punicapp.delivery.nextgen.utils

import androidx.databinding.Observable
import androidx.databinding.ObservableField

fun <T> ObservableField<T>.onChanged(f: ObservableField<T>.(newVal: T?) -> Unit): ObservableField<T> {
    addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
            f(get())
        }
    })
    return this
}
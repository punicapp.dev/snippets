package com.punicapp.delivery.nextgen.utils

import android.location.Geocoder
import com.punicapp.deliveryNext.domain.entities.GeocoderAddress
import com.punicapp.deliveryNext.domain.utils.IGeocoder

class GeocoderImpl(private val geocoder: Geocoder) : IGeocoder {
    override fun geocode(lat: Double, lng: Double, cb: (GeocoderAddress?) -> Unit) {
        val str = geocoder.getFromLocation(lat, lng, 10)?.firstOrNull()?.run {
            if (thoroughfare != null && subThoroughfare != null) "$thoroughfare, $subThoroughfare" else null
        }
        cb(str?.let { GeocoderAddress(it) })
    }
}
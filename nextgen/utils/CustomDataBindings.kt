package com.punicapp.delivery.nextgen.utils

import android.graphics.Paint
import android.net.Uri
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.view.children
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.facebook.drawee.view.GenericDraweeView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.tabs.TabLayout
import com.punicapp.core.utils.DimensUtils.dpToPx
import com.punicapp.core.utils.DimensUtils.pxToDp
import com.punicapp.delivery.nextgen.R
import com.punicapp.deliveryNext.domain.entities.Addon
import com.punicapp.deliveryNext.domain.entities.ProductCategory
import com.punicapp.mvvm.adapters.VmAdapter
import kotlinx.android.synthetic.main.addon_checkbox.view.*
import kotlinx.android.synthetic.main.addon_radio.view.*
import kotlinx.android.synthetic.main.addon_radio.view.price


@BindingAdapter(value = ["layoutManager"], requireAll = false)
fun setLayoutManager(view: RecyclerView, layoutManager: RecyclerView.LayoutManager?) {
    view.layoutManager = layoutManager
}

@BindingAdapter(value = ["spanSizeLookup"], requireAll = false)
fun setSpanSizeLookup(view: RecyclerView, spanSizeLookup: GridLayoutManager.SpanSizeLookup?) {
    val layoutManager = view.layoutManager
    if (layoutManager is GridLayoutManager && spanSizeLookup != null)
        layoutManager.spanSizeLookup = spanSizeLookup
}

@BindingAdapter(value = ["recyclerAdapter"], requireAll = false)
fun setRecyclerAdapter(view: RecyclerView, adapter: VmAdapter?) {
    view.adapter = adapter
}

@BindingAdapter(value = ["recyclerScrollListener", "removeOtherScrollListeners"], requireAll = false)
fun setRecyclerScrollListener(view: RecyclerView, listener: RecyclerView.OnScrollListener?, remove: Boolean? = false) {
    if (remove == true)
        view.clearOnScrollListeners()
    listener?.let { view.addOnScrollListener(it) }
}

@BindingAdapter(value = ["clipToOutline"], requireAll = false)
fun setClipping(view: View, clip: Boolean?) {
    view.clipToOutline = (clip == true)
}

@BindingAdapter(value = ["imageUri"], requireAll = false)
fun setImage(view: GenericDraweeView, uri: String?) {
    uri?.let { Uri.parse(it) }?.let { view.setImageURI(it) }
}

@BindingAdapter(value = ["tabs"], requireAll = false)
fun setTabs(view: TabLayout, tabs: List<ProductCategory>) {
    if (tabs.size <= view.tabCount)
        return

    for (i in view.tabCount until tabs.size) {
        val tab = view.newTab()
        val tabView = LayoutInflater.from(view.context).inflate(R.layout.menu_tab, null) as TextView
        tabView.text = tabs[i].name
        tab.text = tabView.text
        tab.customView = tabView
        view.addTab(tab)
        val tabContainer = (view.getChildAt(0) as ViewGroup).getChildAt(i)
        (tabContainer.layoutParams as ViewGroup.MarginLayoutParams).setMargins(dpToPx(6), 0, dpToPx(6), dpToPx(10))
        tabContainer.setPadding(0, 0, 0, 0)
        tabContainer.requestLayout()
    }
}

@BindingAdapter(value = ["tabSelectedListener", "removeOtherTabSelectionListeners"], requireAll = false)
fun setTabSelectedListener(view: TabLayout, listener: TabLayout.OnTabSelectedListener?, remove: Boolean?) {
    if (remove == true)
        view.clearOnTabSelectedListeners()
    listener?.let { view.addOnTabSelectedListener(it) }
}

@BindingAdapter("textWatcher")
fun setTextWatcher(view: EditText, watcher: TextWatcher) {
    view.addTextChangedListener(watcher)
}

@BindingAdapter(value = ["bottomSheetState"], requireAll = false)
fun setBottomSheetState(view: View, state: Int?) {
    val behavior = BottomSheetBehavior.from(view)
    state?.let { behavior.state = it }
}

interface OnLongClickListener {
    fun onLongClick()
}

@BindingAdapter(value = ["onLongClick"], requireAll = false)
fun onLongClick(view: View, listener: OnLongClickListener) {
    view.setOnLongClickListener {
        listener.onLongClick()
        true
    }
}

@BindingAdapter("underline")
fun setUnderline(textview: TextView, value: Boolean) {
    if (value) {
        textview.paintFlags = textview.paintFlags or Paint.UNDERLINE_TEXT_FLAG
    }
}

interface OnAddonSelectedListener {
    fun onAddonSelected(addon: Addon, selected: Boolean)
}

@BindingAdapter(value = ["addonOptions", "addonListener"], requireAll = false)
fun setOptions(view: LinearLayout, options: List<Addon>?, listener: OnAddonSelectedListener?) {
    if (options.isNullOrEmpty()) {
        view.removeAllViews()
        return
    }

    val inflater = LayoutInflater.from(view.context)
    options.forEach {
        val optionView = inflater.inflate(R.layout.addon_radio, view, false)
        optionView.radio_button.text = it.name
        optionView.price.text = it.price?.toPriceDisplayFormatWithoutSpace()
                ?.let {
                    view.context.getString(R.string.plus_sum, it)
                }
        view.addView(optionView)
        optionView.radio_button.setOnClickListener { buttonView ->
            val checkbox = buttonView as CheckBox
            view.children.forEach {
                if (checkbox.isChecked) {
                    it.radio_button.isChecked = it.radio_button === buttonView
                }
            }
            listener?.onAddonSelected(options[view.indexOfChild(optionView)], checkbox.isChecked)
        }
    }
}

interface OnAddonMultiSelectedListener {
    fun onAddonSelected(addon: Addon, selected: Boolean)
}

@BindingAdapter(value = ["addonMultiOptions", "addonMultiListener"], requireAll = false)
fun setMultiOptions(view: LinearLayout, options: List<Addon>?, listener: OnAddonMultiSelectedListener?) {
    if (options.isNullOrEmpty()) {
        view.removeAllViews()
        return
    }

    val inflater = LayoutInflater.from(view.context)
    options.forEach {
        val optionView = inflater.inflate(R.layout.addon_checkbox, view, false)
        optionView.check_button.text = it.name
        optionView.price.text = it.price?.toPriceDisplayFormatWithoutSpace()
                ?.let {
                    view.context.getString(R.string.plus_sum, it)
                }
        view.addView(optionView)
        optionView.check_button.setOnClickListener { buttonView ->
            listener?.onAddonSelected(options[view.indexOfChild(optionView)], buttonView.check_button.isChecked)
        }
    }
}

interface OnRadioClickListener {
    fun onRadioClick(name: String)
}

@BindingAdapter(value = ["radio_entries", "radio_listener", "chosen_rest"], requireAll = false)
fun setRadioEntries(radioGroup: RadioGroup, entries: List<String>, listener: OnRadioClickListener?, chosenRest: String?) {
    var checked = false
    for (name: String in entries) {
        val rdbtn = RadioButton(radioGroup.context)
        rdbtn.id = View.generateViewId()
        rdbtn.text = name
        rdbtn.setOnClickListener {
            listener?.onRadioClick(name)
        }
        val params = RadioGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        val side = pxToDp(radioGroup.context.resources.getDimension(R.dimen.spacing_16).toInt())
        val top = pxToDp(radioGroup.context.resources.getDimension(R.dimen.spacing_16).toInt())
        params.setMargins(side, top, side, top)
        rdbtn.layoutParams = params
        rdbtn.setPadding(side, top, side, top)
        rdbtn.buttonDrawable = radioGroup.context.getDrawable(R.drawable.radio_button)
        radioGroup.addView(rdbtn)
        if (!checked || (chosenRest != null && chosenRest == name)) {
            rdbtn.isChecked = true
            rdbtn.callOnClick()
            checked = true
        }
    }
}
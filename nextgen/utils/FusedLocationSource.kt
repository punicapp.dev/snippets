package com.punicapp.delivery.nextgen.utils

import android.content.Context
import android.location.Location
import com.google.android.gms.maps.LocationSource
import io.reactivex.disposables.Disposable

class FusedLocationSource(private val context: Context) : LocationSource {
    lateinit var disposable: Disposable
    var onLocationChanged: ((Location) -> Unit)? = null
    var location: Location? = null
        private set

    override fun activate(listener: LocationSource.OnLocationChangedListener) {
        val fusedLocationTracker = FusedLocationTracker(context)
        disposable = fusedLocationTracker.getLocationLatLngObservable()
                .doOnNext {
                    location = it
                    onLocationChanged?.invoke(it)
                }
                .subscribe({
                    listener.onLocationChanged(it)
                }, {})
    }

    override fun deactivate() {
        disposable.dispose()
    }
}

package com.punicapp.delivery.nextgen.utils

class FragmentResultHandler {
    var result: Any? = null

    fun consumeResult(): Any? = result.also { this.result = null }
}